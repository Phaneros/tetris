"use strict";

(function(fileUtils) {
    var loc = window.location.pathname;
    var currentDirectory = loc.substring(0, loc.lastIndexOf('/'));

    fileUtils.loadJson = function(filename, callback) {
        var asynchronous = !!callback;
        var jsonContents;
        var xhr = new XMLHttpRequest();

        xhr.open("get", "file://" + currentDirectory + "/" + filename, asynchronous);
        xhr.onreadystatechange = function() {
            if (xhr.readyState === xhr.DONE) {
                jsonContents = JSON.parse(xhr.responseText);
                asynchronous && typeof callback == "function" && callback(jsonContents);
            }
        }
        asynchronous && (xhr.responseType = "text");
        xhr.send();
        return jsonContents;
    };

    fileUtils.loadTextDocument = function(filename, callback) {
        var asynchronous = !!callback;
        var contents;
        var xhr = new XMLHttpRequest();

        xhr.open("get", "file://" + currentDirectory + "/" + filename, asynchronous);
        xhr.onreadystatechange = function() {
            if (xhr.readyState === xhr.DONE) {
                contents = xhr.responseText;
                asynchronous && typeof callback == "function" && callback(contents);
            }
        }
        asynchronous && (xhr.responseType = "text");
        xhr.send();
        return contents;
    };

})(window.fileUtils = window.fileUtils || {});