"use strict";

(function(glUtils) {
    
    /**
     * Create a compiled gl vertex or fragment shader from plaintext.
     * 
     * @param {!WebGLRenderingContext} gl The WebGL context.
     * @param {!String} program The shader program plaintext.
     * @param {!WebGLEnum} shaderType The type of shader; gl.VERTEX_SHADER or gl.FRAGMENT_SHADER.
     * @param {!String} shaderId The name to refer to this shader by in error messages.
     */
    glUtils.createShader = function(gl, shaderText, shaderType, shaderId="unnamed") {
        var shader = gl.createShader(shaderType);
        gl.shaderSource(shader, shaderText);
        gl.compileShader(shader);
        if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
            var info = gl.getShaderInfoLog(shader);
            console.error("ERROR::Could not compile WebGL Shader: " + shaderId + " \n\n" + info);
            throw "ERROR::Could not compile WebGL Shader: " + shaderId + " \n\n" + info;
        }

        return shader;
    };

    /**
     * Create a gl shader program from two uncompiled shader programs.
     * 
     * @param {!WebGLRenderingContext} gl The WebGL context.
     * @param {!WebGLProgram} program The WebGL program to format.
     * @param {!String} vertexShader The plaintext of the vertex shader program.
     * @param {!String} fragmentShader The plaintext of the fragment shader program.
     */
    glUtils.compileProgram = function(gl, program, vertexShader, fragmentShader) {
        var vertShader = glUtils.createShader(gl, vertexShader, gl.VERTEX_SHADER, "VertexShader");
        var fragShader = glUtils.createShader(gl, fragmentShader, gl.FRAGMENT_SHADER, "FragmentShader");

        gl.attachShader(program, vertShader);
        gl.attachShader(program, fragShader);

        gl.linkProgram(program);
        if (!gl.getProgramParameter(program, gl.LINK_STATUS)) {
            var info = gl.getProgramInfoLog(program);
            throw "ERROR::Could not compile WebGL program.\n\n" + info;
        }
    };

    /**
     * Create a gl shader from the HTML shader script id.
     * 
     * @param {!WebGLRenderingContext} gl The WebGL context.
     * @param {!WebGLShader} shader The shader object to be formatted.
     * @param {!String} shaderId The id of the shader script in the HTML document.
     */
    glUtils.compileShaderFromId = function(gl, shader, shaderId) {
        // get script
        var shaderScript = document.getElementById(shaderId);
        if (!shaderScript) {
            throw "ERROR::Unable to find shader script id: " + shaderId;
        }
        var shaderSource = shaderScript.textContent;
        
        // load script and compile shader object
        gl.shaderSource(shader, shaderSource)
        gl.compileShader(shader);
        if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
            var info = gl.getShaderInfoLog(shader);
            throw "ERROR::Could not compile WebGL Shader: " + shaderId + " \n\n" + info;
        }
    };

    glUtils.loadShadersHTML = function(gl, shaderlist) {
        var shaders = [];
        for (var i = 0; i < shaderlist.length; ++i) {
            shaders.push({});
            shaders[i] = gl.createShader(
                document.getElementById(shaderlist[i]).type === "x-shader/x-vertex" ?
                gl.VERTEX_SHADER : gl.FRAGMENT_SHADER
            );
            glUtils.compileShaderFromId(
                gl, shaders[i], shaderlist[i]);
        }
        return shaders;
    };

    /**
     * Create a gl program from two shader ids.
     * 
     * @param {!WebGLRenderingContext} gl The WebGL context.
     * @param {!WebGLProgram} program The webGL program to be loaded with shaders.
     * @param {!String} vertexShaderId The id of the vertex shader script in the HTML document.
     * @param {!String} fragmentShaderId The id of the frag shader script in the HTML document.
     */
    glUtils.formatProgramFromShaderIds = function(gl, program, vertexShaderId, fragmentShaderId) {
        var vertexShader = gl.createShader(gl.VERTEX_SHADER);
        var fragmentShader = gl.createShader(gl.FRAGMENT_SHADER);
        glUtils.compileShaderFromId(gl, vertexShader, vertexShaderId);
        glUtils.compileShaderFromId(gl, fragmentShader, fragmentShaderId);
        gl.attachShader(program, vertexShader);
        gl.attachShader(program, fragmentShader);

        gl.linkProgram(program);
        if (!gl.getProgramParameter(program, gl.LINK_STATUS)) {
            var info = gl.getProgramInfoLog(program);
            throw "ERROR::Could not compile WebGL program.\n\n" + info;
        }

    };

}(window.glUtils = window.glUtils || {}));


