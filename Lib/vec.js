"use strict";

(function(vec) {

    // vector add
    vec.addvec = function() {
        return Array.prototype.slice.call(arguments)
            .reduce(
                (acc, cur) => acc.map((x, i) => x + cur[i])
                , Array(arguments[0].length).fill(0)
            );
    }

    // element-wise multiplication
    vec.multvec = function() {
        return Array.prototype.slice.call(arguments)
            .reduce(
                (acc, cur) => acc.map((x, i) => x * cur[i])
                , Array(arguments[0].length).fill(1)
            );
    }

    // vector subtract
    vec.subtractvec = function() {
        return Array.prototype.slice.call(arguments, 1)
            .reduce((acc, cur) => acc.map((x, i) => x - cur[i])
            , arguments[0]);
    };

    // vector dot product
    vec.dot = function() {
        return vec.multvec(arguments).reduce((acc, cur) => acc + cur, 0);
    };

    // matrix multiplication
    vec.I = [[1,0], [0,1]];
    vec.multm22v2 = function(M, u) {
        return [
            M[0][0] * u[0] + M[1][0] * u[1],
            M[0][1] * u[0] + M[1][1] * u[1]
        ];
    };

    // componentwise matrix multiplication
    vec.componentMultm = function(A, B) {
        if (A.length != B.length) {
            console.error(`componentMultm(): matrix dimensions don't match (${A.length}, ${B.length})`);
            throw `componentMultm()::dimensions don't match`;
        }

        let C = [] 
        for (let i = 0; i < A.length; ++i) {
            C = C.concat([]);
            for (let j = 0; j < A[i].length; ++j) {
                C[i] = C[i].concat(A[i][j] * B[i][j]);
            }
        }
        return C;
    };

    vec.transposemat = function(A) {
        return Array(A[0].length)
            .fill()
            .map((x, i) => Array(A.length)
                            .fill()
                            .map((x, j) => A[j][i]));
    };

    /**
     * Slice a matrix, returning a matrix that is a copy of the rectangular region requested.
     * 
     * @param {Number[][]} M The source matrix.
     * @param {Number[]} from The coordinates of the top-left entry in M to include in the slice.
     * @param {Number[]} to The coordinates of the bottom-right entry in M to include in the slice.
     *
     **/ 
    vec.slicem = function(M, from, to) {
        return M.slice(from[0], to[0]+1).map((x) => x.slice(from[1], to[1]+1));
    };

    /**
     * Check if matrices are equal.
     */
    vec.equalmat = function(A, B) {
        if (A.length !== B.length || (A.length && (A[0].length !== B[0].length))) {
            return false;
        }
        for (let x = 0; x < A.length; ++x) {
            for (let y = 0; y < A.length; ++y) {
                if (A[x][y] !== B[x][y]) {
                    return false;
                }
            }
        }
        return true;
    };
    
    /**
    * Check if vectors are equal.
    */
    vec.equalv = function(u, v) {
        if (u.length !== v.length) {
            return false;
        }
        for (let x = 0; x < u.length; ++x) {
            if (u[x] !== v[x]) {
                return false;
            }
        }
        return true;
    };

})(window.vec = window.vec || {});