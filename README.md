Tetris

Author: Matthew Marinets

All code in this package is written by me.
Images used in this project were created by me using Aseprite.

The principle: "One does not simply half-ass Tetris"

    Configuration Note
-----------------------

Mozilla changed the behaviour of fetching files locally a few months after I first made this project, breaking the fetching of local files.

To fix this issue, go to about:config, and change the privacy.file_unique_origin option to false.

    Default Controls
-----------------------
-Move Piece Left:   [A, J, LeftArrow]

-Move Piece Right:  [D, L, RightArrow]

-Rotate Piece:      [W, I, UpArrow]

-Soft drop piece:   [S, K, DownArrow]

-Hard drop piece:   [Spacebar]

-Menu Up:           [UpArrow]

-Menu Down:         [DownArrow]

-Menu Select:       [Enter]

-Pause/Unpause:     [Escape]

-Save state:        [F1]

-Load saved state:  [F2]


    Additional Features
-----------------------
-Piece textures and palettes

    -All pieces have a different palette associated with them
    
    -Which piece gets which palette is adjustable in the "Palettes" menu.
    
    -The current palette assignments are viewable from a pause or game over menu with the "Pieces"
     button.
     
    -Changes made in the Palettes menu are persistent across sessions.
    
-A piece preview to see the next piece incoming.

-Menus and text display

-Scores and levels

    -More rows made at once award more points.
    
    -This base score is multiplied by the level.
    
    -The level increases with points, with levelups happening at quadratic point intervals.
    
    -The piece falling speed increases with level.
    
-A persistent highscore leaderboard
    
-Savestates

    -Press F1 to save the current application state.
    
    -Press F2 to load the last saved application state.
    
    -This functionality works even in menus.
    
    -This functionality depends on the application state object being fully serializable.


    Resources
-----------------------
-I used Aseprite to draw all my textures:
 https://www.aseprite.org/
 
-Mozilla's WebGL API documentation:
 https://developer.mozilla.org/en-US/docs/Web/API/WebGL_API
 
-This post on StackOverflow gave me the idea to use two textures to implement a palette:
 https://stackoverflow.com/a/19719654
 
-WebGLFundamentals.org had a good resource for getting textures to work:
 https://webglfundamentals.org/webgl/lessons/webgl-3d-textures.html

