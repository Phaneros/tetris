precision mediump float;

varying vec2 v_TexCoord;
varying float v_PaletteIndex;
varying float v_FadeFactor;

uniform sampler2D u_Tilesheet;
uniform sampler2D u_Palette;

void main() {
    vec2 flooredCoord = mod(v_TexCoord, vec2(192.0, 320.0));

    // sample from the palette, using the tileSheet texture to get an index
    vec4 colour = texture2D(
        u_Palette,
        vec2(
            texture2D(u_Tilesheet, flooredCoord).a + 0.001953125,
            v_PaletteIndex
        )
    ) * vec4(v_FadeFactor, v_FadeFactor, v_FadeFactor, 1.0);
    if (colour.a <= 0.5) {
        discard;
    }
    gl_FragColor = colour;
}