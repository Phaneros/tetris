
attribute vec2 a_Position;
attribute vec2 a_TexCoord;
attribute float a_PaletteIndex;
attribute float a_FadeFactor;

varying vec2 v_TexCoord;
varying float v_PaletteIndex;
varying float v_FadeFactor;

void main() {
    gl_Position = vec4(a_Position, 0.0, 1.0);
    v_TexCoord = a_TexCoord;
    v_PaletteIndex = a_PaletteIndex;
    v_FadeFactor = a_FadeFactor;
}