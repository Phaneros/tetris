
(function runTests() {
    (function matrixEqualTest() {
        let passed = true;
        let matrixA = [[1,2,3],
                       [4,5,6],
                       [7,8,9]];
        let matrixB = [[1,2,3],
                       [4,5,6],
                       [7,8,9]];
        if (!vec.equalmat(matrixA, matrixB)) {
            console.error("matrixEqualTest()::Test Failed D: (1)");
            passed = false;
        }
        matrixB[1][1] = 60;
        if (vec.equalmat(matrixA, matrixB)) {
            console.error("matrixEqualTest()::Test Failed D: (2)");
            passed = false;
        }
        passed && console.log("matrixEqualTest()::Test Passed :D")

    })();

    // test vec slice
    (function matrixSliceTest() {
        let passed = true;
        let matrix = [[1,2,3],
                        [4,5,6],
                        [7,8,9]];
        let all = vec.slicem(matrix, [0,0], [2,2]);
        if (!vec.equalmat(matrix, all)) {
            console.error("matrixSliceTest()::Test Failed D: (1)");
            passed = false;
        }
        let botRight = vec.slicem(matrix, [1,1], [2,2]);
        if (
            botRight[0][0] !== matrix[1][1]
            || botRight[0][1] !== matrix[1][2]
            || botRight[1][0] !== matrix[2][1]
            || botRight[1][1] !== matrix[2][2]) {
                console.error("matrixSliceTest()::Test Failed D: (2)");
                passed = false;
        }

        let matrix2 = [
            [0, 0, 0, 1],
            [0, 0, 0, 1],
            [0, 0, 1, 1],
            [0, 0, 1, 2]
        ];
        let expected = [
            [0, 1],
            [0, 1],
            [1, 1]
        ];
        let slice = vec.slicem(matrix2, [0, 2], [2, 3])
        if (!vec.equalmat(slice, expected)) {
            console.error("matrixSliceTest()::Test Failed D: (3)");
            console.error(`matrixSliceTest()::got ${slice}, expected ${expected}`);
            passed = false;
        }

        passed && console.log("matrixSliceTest()::Test Passed :D");
    })();

    // test addvec
    (function addvecTest() {
        let passed = true;
        let u = [1,2,3,4];
        let sum = vec.addvec(u, u);
        if (!vec.equalv(u, u)) {
            console.error("addvecTest()::Test Failed D: (1)");
            passed = false;
        }
        if (!vec.equalv(sum, [2,4,6,8])) {
            console.error("addvecTest()::Test Failed D: (2)");
            console.error(`addvecTest()::Expected ${[2,4,6,8]}, got ${sum}`);
            passed = false;
        }
        passed && console.log("addvecTest()::Test Passed :D");
    })();

    // text subtractvec
    (function subtractvecTest() {
        let passed = true;
        let u = [1,2,3,4];
        let difference = vec.subtractvec(u, u);
        let expected = [0, 0, 0, 0];
        if (!vec.equalv(difference, expected)) {
            console.error("subtractvecTest()::Test Failed D: (1)");
            console.error(`subtractvecTest()::Expected ${expected}, got ${difference}`);
            passed = false;
        }
        passed && console.log("subtractvecTest()::Test Passed :D");
    })();
    
    // test multvec
    (function multvecTest() {
        let u = [1,2,3,4];
        let product = vec.multvec(u, u);
        let expected = [1,4,9,16];
        if (!vec.equalv(product, expected)) {
            console.error("multvecTest()::Test Failed D:");
            console.error(`multvecTest()::Expected ${expected}, got ${product}`);
        } else {
            console.log("multvecTest()::Test Passed :D");
        }
    })();

    // test dot
    (function dotTest() {
        let u = [1,2,3,4];
        let dot = vec.dot(u, u);
        let expected = [1,4,9,16].reduce((acc, cur) => acc + cur, 0);
        if (!vec.equalv(dot, expected)) {
            console.error("dotTest()::Test Failed D:");
            console.error(`dotTest()::Expected ${expected}, got ${dot}`);
        } else {
            console.log("dotTest()::Test Passed :D");
        }
    })();

    // test transpose
    (function transposematTest() {
        let passed = true;
        let matrix = [[1,2,3],
                        [4,5,6],
                        [7,8,9]]
        let transpose = vec.transposemat(matrix);
        let expected = [[1,4,7],
                        [2,5,8],
                        [3,6,9]];
        if (!vec.equalmat(transpose, expected)) {
            console.error("transposematTest()::Test Failed D: (1)");
            console.error(`transposematTest()::Expected ${expected}, got ${transpose}`);
            passed = false;
        }
        passed && console.log("transposematTest()::Test Passed :D");
    })();

})();
