"use strict";

(function(setup) {
    setup.appLogicSetup = new Promise((resolve) => {
        Promise.all([
            setup.gameLogicSetup,
            setup.menuLogicSetup,
            setup.optionsSetup,
            setup.gameRenderSetup,

        ]).then((inputs) => {
            let [game, menu, options, render] = inputs;

            // global state for the whole application
            let state = {};

            // variables used by event listeners
            let pressedKeys = {};
            let textInputValidKeys = /^[A-Z\d~+*!?.:>\-=_]$/i;

            /**
             * Initializes event listeners to register keypresses in the gameState.
             * @param {!Object} pressedKeys - The object in which to register what keys are being held.
             */
            let initializeHeldKeyListeners = function(pressedKeys) {
                document.addEventListener("keydown", (keyEvent) => {
                    pressedKeys[keyEvent.key] = true;
                });
                document.addEventListener("keyup", (keyEvent) => {
                    pressedKeys[keyEvent.key] = false;
                });
            };
            initializeHeldKeyListeners(state.pressedKeys = pressedKeys);
            
            /**
             * Initializes event listeners to register keypresses for the purposes of text input.
             * @param {!Object} textInputField - The object in which to log text.
             */
            let initializeTextInputListener = function(textInputStack) {
                let eventListenerFunc = function(keyEvent) {
                    if (textInputStack.text.length) {
                        let last = textInputStack.text.length-1;
                        // maxLength === 0 marks an entry as being unused. Pop it and continue.
                        while (last >= 0 && textInputStack.maxLength[last] === 0) {
                            textInputStack.maxLength.pop();
                            textInputStack.text.pop();
                            --last;
                        }
                        if (!textInputStack.text.length) {
                            // check if we've popped the entire stack.
                            return;
                        }
                        if (
                            keyEvent.key === "Backspace"
                            && textInputStack.text[last]
                            && textInputStack.text[last].length > 0
                        ) {
                            textInputStack.text[last] = textInputStack.text[last].slice(0, textInputStack.text[last].length-1);
                        } else if (
                            keyEvent.key.match(textInputValidKeys)
                            && textInputStack.text[last].length < textInputStack.maxLength[last]
                        ) {
                            textInputStack.text[last] += keyEvent.key;
                        }
                    }
                };
                document.addEventListener("keydown", eventListenerFunc);
                return eventListenerFunc;
            };
            let textInputListenerFunc = initializeTextInputListener(state.textInputStack = {text: [], maxLength: []});
            
            let saveStates = Array(3).fill();
            let saveState = function(state, slot) {
                saveStates[slot] = JSON.stringify(state);
            };

            let loadState = function(slot) {
                if (saveStates[slot]) {
                    state = JSON.parse(saveStates[slot]);
                    // pressed keys can be transferred cleanly
                    state.pressedKeys = pressedKeys;

                    // the input field stack should be preserved, but the event listener should be reassigned.
                    document.removeEventListener("keydown", textInputListenerFunc);
                    textInputListenerFunc = initializeTextInputListener(state.textInputStack);
                } else {
                    console.log(`loadState()::slot ${slot} is empty`);
                }
            };

            let lastMenuToggle = 0;
            let lastSaveState = 0;
            let appLogic = {
                /**
                 * Ticks the entire application by one frame.
                 * @param {Number} now - The current time gotten from window.requestAnimationFrame.
                 * @returns {RenderData} A structure of data to put in the vertex attribute buffers.
                 */
                tick: function(now) {
                    // if there is no gameState or menuState, default to main menu
                    if (!state.gameState && !state.menuState) {
                        state.menuState = menu.newMenu.mainMenu(now);
                    }

                    // menuState management.
                    if (
                        state.gameState
                        && options.inputUtils.checkCommand(options.keys.MENU, state.pressedKeys)
                        && (now - lastMenuToggle > options.menu.menuToggleRefreshSpeed)
                    ) {
                        // set pause menu
                        if (state.menuState) {
                            menu.leaveMenu(state);
                        } else {
                            state.menuState = menu.newMenu.pauseMenu(now, state.gameState.board);
                        }
                        lastMenuToggle = now;
                    }
                    if (!state.menuState && state.gameState && state.gameState.gameOver) {
                        // set game over menu
                        state.menuState = menu.newMenu.gameOver(now, state)
                    }

                    // save state stuff.
                    if (
                        options.inputUtils.checkCommand(options.keys.SAVE_SLOT_1, state.pressedKeys)
                        && (now - lastSaveState > options.menu.saveStateRefreshSpeed)
                    ) {
                        saveState(state, 1);
                        lastSaveState = now;
                    }
                    if (
                        options.inputUtils.checkCommand(options.keys.LOAD_SLOT_1, state.pressedKeys)
                        && (now - lastSaveState > options.menu.saveStateRefreshSpeed)
                    ) {
                        loadState(1);
                        lastSaveState = now;
                    }

                    // return render information.
                    if (!state.menuState) {
                        game.tick(state);
                        return render.getRenderData(state.gameState);
                    } else {
                        menu.tick(state, now);
                        return render.getRenderData(state.menuState);
                    }
                },
            };

            resolve(appLogic);
        }).catch((error) => console.error(error));
    }).catch((error) => console.error(error));
})(window.setup = window.setup || {});

