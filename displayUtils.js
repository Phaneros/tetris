(function(setup) {
    setup.displayUtilsSetup = new Promise((resolve) => {
        Promise.all([
            resources.loadTileSheetInfo,
            resources.loadDisplayOptions,
            setup.optionsSetup
        ]).then((inputs) => {
            let [tileSheet, displayOpts, options] = inputs;

            let characterMap = new Map([
                ["0", "ZERO"],
                ["1", "ONE"],
                ["2", "TWO"],
                ["3", "THREE"],
                ["4", "FOUR"],
                ["5", "FIVE"],
                ["6", "SIX"],
                ["7", "SEVEN"],
                ["8", "EIGHT"],
                ["9", "NINE"],
                [".", "PERIOD"],
                ["!", "EXCLAMATION_MARK"],
                ["?", "QUESTION_MARK"],
                [":", "COLON"],
                [">", "CURSOR"],
                ["*", "STAR"],
                ["~", "TREMBLE"],
                ["+", "PLUS"],
                ["-", "MINUS"],
                ["=", "EQUALS"],
                ["_", "UNDERSCORE"],
                ["@", "COPYRIGHT"]
            ]);
            let displayUtils = {
                /**
                 * Converts a single character into a sprite index.
                 * @param {character} character The character to be converted.
                 * @returns {SpriteIndex|Boolean} The character's corresponding sprite index, or false for a space.
                 */
                character2SpriteIndex: function(character) {
                    if (character.match(/[A-Z]/i)) {
                        return tileSheet.tileMap[character].coord;
                    } else if (character === " ") {
                        return false;
                    } else {
                        let tileName = characterMap.get(character);
                        return tileName && tileSheet.tileMap[tileName].coord || tileSheet.tileMap["DEFAULT"].coord;
                    }
                },

                /**
                 * Converts a string into a list of sprite indices.
                 * @param {String} message The string to be converted.
                 * @returns {SpriteIndex[]} The corresponding sprite indices for each character.
                 */
                string2SpriteIndices: function(message) {
                    let spriteIndices = [];
                    for (let i = 0; i < message.length; ++i) {
                        let character = message[i].toUpperCase();
                        spriteIndices.push(this.character2SpriteIndex(character));
                    }
                    return spriteIndices;
                },

                /**
                 * Converts a board into a 2D list of sprite indices and a 2D list of palette indices.
                 * @param {Object[][]} tiles - The current game board's tiles.
                 * @returns {[SpriteIndex, PaletteIndex, Number]} [spriteIndices, paletteIndices, fades].
                 */
                board2SpriteIndicesAndPalettes: function(tiles) {
                    let spriteIndices = [];
                    let paletteIndices = [];
                    let fades = [];
                    for (let i = 0; i < tiles.length; ++i) {
                        spriteIndices.push([]);
                        paletteIndices.push([]);
                        fades.push([]);
                        for (let j = 0; j < tiles[i].length; ++j) {
                            spriteIndices[i].push(tiles[i][j].spriteIndex);
                            paletteIndices[i].push(tiles[i][j].paletteIndex);
                            fades[i].push(tiles[i][j].fade == null ? -1 : tiles[i][j].fade);
                        }
                    }
                    return [spriteIndices, paletteIndices, fades];
                },

                /**
                 * Get a copy of a display options object for rendering strings.
                 * @param {!String} type - The key of the desired object
                 * @returns {RenderOptions} A render settings object.
                 */
                getDisplayOptions: function(type) {
                    if (!displayOpts[type]) {
                        console.error(`getDisplayOptions()::Tried to access displayOptions for nonexistent key: ${type}`);
                        return {};
                    }
                    return JSON.parse(JSON.stringify(displayOpts[type]));
                },

                /**
                 * Check the options used to render strings, and make the object ready to use.
                 * @param {RenderOptions} opts - The options object to be checked.
                 * @returns {RenderOptions} The checked options object.
                 */
                formatRenderOptions: function(opts) {
                    if (opts == null) {
                        opts = {};
                    }
                    if (opts.palette == null) {
                        opts.palette = displayOpts.default.palette;
                    }
                    if (opts.fontSize == null) {
                        opts.fontSize = displayOpts.default.fontSize;
                    }
                    if (opts.position == null) {
                        opts.position = displayOpts.default.position;
                    }
                    if (opts.fontSize.mode === "pixels") {
                        opts.fontSize.w *=  options.canvasSizes.pixel.w;
                        opts.fontSize.h *=  options.canvasSizes.pixel.h;
                        opts.fontSize.dx *= options.canvasSizes.pixel.w;
                        opts.fontSize.dy *= options.canvasSizes.pixel.h;
                        opts.fontSize.mode = "normalized";
                    }
                    if (opts.fade == null) {
                        opts.fade = displayOpts.default.fade;
                    }
                    return opts;
                },
                
                /**
                 * Sets unassigned fields in the child's renderOptions to be equal to those of the parent.
                 * WARNING - Copied elements are shallow copies - changes to nested elements on either the
                 *           parent or the child will affect both.
                 * @param {RenderOptions} child - The RenderOptions object to update.
                 * @param {RenderOptions} parent - The RenderOptions object to draw elements from.
                 */
                inheritRenderOptions: function(child, parent) {
                    if (child.palette == null) {
                        child.palette = parent.palette;
                    }
                    if (child.fontSize == null) {
                        child.fontSize = parent.fontSize;
                    }
                    if (child.position == null) {
                        child.position = parent.position;
                    }
                    if (child.fade == null) {
                        child.fade = parent.fade;
                    }
                    return child;
                }
            };
            
            resolve(displayUtils);
        }).catch((error) => console.error(error));
    }).catch((error) => console.error(error));
})(window.setup = window.setup || {});