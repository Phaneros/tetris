"use strict";

(function(setup) {
    setup.gameLogicSetup = new Promise((resolve) => {
        Promise.all([
            resources.loadTetrominos,
            resources.loadTileSheetInfo,
            setup.optionsSetup,
            setup.displayUtilsSetup
        ]).then((inputs) => {
            let [tetrominos, tileSheet, options, displayUtils] = inputs;
            
            const opts = options.game;

            const down = [0, -1];
            const right = [1, 0];
            const left = [-1, 0];
            const pieceRotateMatrix = [[0, 1],
                                       [-1, 0]];    // rotates counterclockwise
            
            let tileNames = Object.keys(tileSheet.tileMap); // a list of names of all registered tiles in the tileset
            
            // Generate a string from the tile types that we can use to determine a piece's tiles.
            // Format: [TILE_NAME][lookupCode] * the number of registered tiles.
            // The lookup code is a string characters from {1, 0, x}
            let tileTypeSearchString = "";
            for (let i = 0; i < tileNames.length; ++i) {
                if (tileSheet.tileMap[tileNames[i]].regex) {
                    tileTypeSearchString = (tileTypeSearchString
                        .concat(tileNames[i])
                        .concat(tileSheet.tileMap[tileNames[i]].regex)
                        );
                }
            }

            let boardTools = {
                /**
                 * Creates a new empty tile object.
                 * @returns {BoardTile} An empty board tile.
                 */
                getNewEmptyTile: function() {
                    return {
                        spriteIndex: opts.emptyTileSprite,
                        paletteIndex: options.unassignedPalette,
                        filled: false
                    };
                },

                /**
                 * Gets a clean board of empty tiles.
                 * @param {Number} width - The width of the board.
                 * @param {Number} height - The height of the board.
                 */
                getNewBoard: function(width, height) {
                    return {
                        tiles: Array(width).fill().map(
                            () => Array(height).fill().map(
                                this.getNewEmptyTile
                            ))
                    };
                },

                /**
                 * Checks if a row in a board is completely filled.
                 * @param {GameBoard} board - The board to check.
                 * @param {Number} y - The board row to check.
                 * @returns {Boolean} True if the row is filled.
                 */
                checkRowFill: function(board, y) {
                    for (let x = 0; x < options.boardWidth; ++x) {
                        if (!board.tiles[x][y].filled) {
                            return false;
                        }
                    }
                    return true;
                },

                /**
                 * Put the active piece's tile data into the board.
                 * @param {!ActivePiece} piece - The piece to add.
                 * @param {!GameBoard} board - The board to add the piece's tile data to.
                 */
                addPieceToBoard: function(piece, board) {
                    for (let i = 0; i < piece.tileDeltas.length; ++i) {
                        let coords = vec.addvec(piece.position, piece.tileDeltas[i]);
                        if (coords[0] < 0 || coords[1] < 0 || coords[0] >= board.tiles.length || coords[1] >= board.tiles[0].length) {
                            continue;
                        }
                        board.tiles[coords[0]][coords[1]].spriteIndex = piece.tileSprites[i];
                        board.tiles[coords[0]][coords[1]].paletteIndex = options.colourAssignment[piece.pieceType.id];
                    }
                },

                /**
                 * Remove the active piece's tile data from the board.
                 * @param {!ActivePiece} piece - The piece to remove.
                 * @param {!GameBoard} board - The board to add the piece's tile data to.
                 */
                removePieceFromBoard: function(piece, board) {
                    for (let i = 0; i < piece.tileDeltas.length; ++i) {
                        let coords = vec.addvec(piece.position, piece.tileDeltas[i]);
                        if (coords[0] < 0 || coords[1] < 0 || coords[0] >= board.tiles.length || coords[1] >= board.tiles[0].length) {
                            continue;
                        }
                        board.tiles[coords[0]][coords[1]].spriteIndex = opts.emptyTileSprite;
                        board.tiles[coords[0]][coords[1]].paletteIndex = options.unassignedPalette;
                    }
                },

                checkCollision: function(board, tiles, position=[0, 0]) {
                    for (let i = 0; i < tiles.length; ++i) {
                        let checkCoords = vec.addvec(position, tiles[i]);
                        if (checkCoords[1] >= options.boardHeight) {
                            // allow rotation even when at the top of the board.
                            return false;
                        }
                        if (
                            checkCoords[1] < 0 || checkCoords[0] < 0
                            || checkCoords[0] >= options.boardWidth
                            || board.tiles[checkCoords[0]][checkCoords[1]].filled
                        ) {
                            return true;
                        }
                    }
                    return false;
                },

                /**
                 * Gets a board to preview all pieces with all different colour palettes.
                 * @param {Number} width - The width of the board.
                 * @param {Number} height - The height of the board.
                 */
                getPiecePreviewBoard: function(width, height) {
                    let board = boardTools.getNewBoard(width, height);
                    let distances = tetrominos.info.maxDistance;
                    let curX = distances.left;
                    let curY = board.tiles[0].length - 1 - distances.up;
                    for (let palette = 0; palette < tetrominos.pieceTypes.length; ++palette) {
                        for (let pieceId = 0; pieceId < tetrominos.pieceTypes.length; ++pieceId) {
                            if (curX > board.tiles.length) {
                                curX = distances.left;
                                curY -= distances.up + distances.down + 1;
                            }
                            let piece = rollNewPiece(pieceId);
                            piece.position = [curX, curY];
                            piece.paletteIndex = palette;
                            updateSpriteTable(piece);
                            boardTools.addPieceToBoard(piece, board);
                            curX += distances.left + distances.right;
                        }
                    }
                    return board;
                },

                /**
                 * Gets a board to preview all pieces with all different colour palettes.
                 * @param {Number} width - The width of the board.
                 * @param {Number} height - The height of the board.
                 */
                getPalettePreviewBoard: function() {
                    let distances = tetrominos.info.maxDistance;
                    let board = boardTools.getNewBoard(
                        distances.left + distances.right + 1,
                        (distances.up + distances.down + 1) * tetrominos.pieceTypes.length
                    );
                    let x = distances.left;
                    let curY = board.tiles[0].length - 1 - distances.up;
                    for (let pieceId = 0; pieceId < tetrominos.pieceTypes.length; ++pieceId) {
                        let piece = rollNewPiece(pieceId);
                        piece.position = [x, curY];
                        updateSpriteTable(piece);
                        boardTools.addPieceToBoard(piece, board);
                        curY -= distances.up + distances.down + 1;
                    }
                    return board;
                }
            };

            let animationTools = {
                /**
                 * Creates an animation object that can be ticked with animationLogic.tick().
                 * @param {Number} duration - The length of time the animation should last.
                 * @returns {AnimationState} The animation object.
                 */
                getNewAnimation: function(duration) {
                    return {
                        completion: 0,
                        totalDuration: duration,
                        rows: []
                    };
                },

                /**
                 * Ticks an AnimationState object one time increment.
                 * @param {AnimationState} animationState - The AnimationState object to tick.
                 * @param {GameState} gameState - The GameState object to apply the animation to.
                 * @returns {Boolean} Returns false if the animation is completed.
                 */
                tick: function(animationState, gameState) {
                    if (animationState.completion >= animationState.totalDuration) {
                        return false;
                    }
                    for (let y = 0; y < animationState.rows.length; ++y) {
                        let row = animationState.rows[y];
                        for (let col = 0; col < gameState.board.tiles.length; ++col) {
                            // gameState.board.tiles[col][row].spriteIndex = [9,4];
                            gameState.board.tiles[col][row].fade = 
                                1 - animationState.completion / animationState.totalDuration;
                        }
                    }
                    ++animationState.completion;
                    return true;
                }
            };

            /**
             * Updates the text field of a GameStats object.
             * @param {GameStats} stats - The GameStats object to update.
             * @returns {GameStats} The resulting stats object.
             */
            let updateStatsText = function(stats) {
                stats.text = [
                    "Score:".concat(stats.score.toString().padStart(5, " ")),
                    "Level:".concat(stats.level.toString().padStart(5, " ")),
                    " Rows:".concat(stats.rows.toString().padStart(5, " ")),
                ];
                return stats;
            }

            /**
             * Creates a new game stats structure.
             * @returns {GameStats} An empty gameStats object.
             */
            let getNewStats = function() {
                return updateStatsText({
                    score: 0,
                    level: 1,
                    piecesLanded: 0,
                    rows: 0,
                    text: "",
                    options: displayUtils.getDisplayOptions("scoreboard")
                });
            };

            let log = {
                 /**
                 * Creates a new log object.
                 * @returns {Log} An empty log object.
                 */
                getNew: function() {
                    return {
                        lastUpdate: Infinity,
                        text: false,
                        options: displayUtils.getDisplayOptions("log")
                    }
                },

                /**
                 * Writes a message to the log.
                 * @param {!GameState} gameState - The current gameState.
                 * @param {String} text - The text to be written.
                 * @param {PaletteIndex=} palette - The palette to use for the message.
                 */
                write: function(gameState, text, palette) {
                    gameState.log.text = text;
                    gameState.log.lastUpdate = gameState.currentFrame;
                    palette && (gameState.log.options.palette = palette);
                },

                /**
                 * Checks whether the log message is out of date and erases the text if it is.
                 * @param {GameState} gameState - The current gameState. 
                 */
                tick: function(gameState) {
                    if (gameState.log.text) {
                        gameState.log.options.fade = 1 - (gameState.currentFrame - gameState.log.lastUpdate) / opts.logDisplayTime;
                        if (gameState.log.options.fade <= 0.0) {
                            gameState.log.lastUpdate = Infinity;
                            gameState.log.text = false;
                        }
                    }
                }
            };

            /**
             * Update the stats after having just landed a piece, and write to the log.
             * @param {!GameState} gameState - The current gameState. 
             * @param {!Number=} rows - The number of rows that were completed when this piece landed.
             * @returns {GameStats} The updated stats object. 
             */
            let addStats = function(gameState, numRows=0) {
                let stats = gameState.stats;
                ++stats.piecesLanded;
                stats.rows += numRows;
                let points = (numRows < opts.pointsPerRow.length ? opts.pointsPerRow[numRows]: 16) * stats.level;
                stats.score += points;
                stats.level = Math.floor(stats.rows / 10) + 1;
                // stats.level = Math.ceil((stats.score + 1) / opts.pointsToLevel);
                gameState.fallingSpeed = Math.max(opts.maxFallSpeed, opts.startingSpeed + 1 - stats.level);
                if (opts.messages[numRows]) {
                    log.write(
                        gameState,
                        opts.messages[numRows].padEnd(15, " ").concat("+" + points.toString()),
                        opts.messageColours[numRows]);
                }
                return updateStatsText(stats);
            };

            /**
             * Returns a TileInfo object for that would fit a local map of tiles.
             * @param {!Number[][]} localMap A 3x3 map of which tiles in the region are occupied.
             * @param {!TileInfo} localMap A 3x3 map of which tiles in the region are occupied.
             */
            let getFirstMatchingSpriteTile = function(localMap) {
                // First we must turn the localMap (a 2d list) into a regex so we can search
                // for the tile.
                // The regex format is ([A-Z_]+)[x0][x0][x0][x0][x0][x0][x0][x0][x0]
                // (For an all-0 localMap)
                // The actual representation is lists of columns, but the regexes want a 
                // string in left-to-right, top-to-bottom format; getting that is tricky.
                let localMapString = "([A-Z_]+)".concat(localMap.reduce(
                    (acc, cur) => acc.map(
                            (x, i) => x.concat("[x".concat(cur[i].toString()).concat("]"))
                        ),
                    Array(3).fill("")
                ).reverse().join(""));

                let results = tileTypeSearchString.match(RegExp(localMapString));
                if (results.length < 2) {
                    console.error(`getFirstMatchingSpriteTile()::Failed to find tile in tile lookup with string ${localMap}`);
                    results = ["", "DEFAULT"]
                }

                return tileSheet.tileMap[results[1]];
            }

            /**
             * Update the active piece's tile data information.
             * @param {!Piece} activePiece The piece to update.
             */
            let updateSpriteTable = function(activePiece) {
                let radius = tetrominos.info.maxRadius + 1;  // add 1 for padding around outermost square
                activePiece.tileSprites = [];

                // Construct a 2d map of tiles;
                // 1 means the square is occupied, 0 means it's empty.
                // Tile [0,0] will be in map[radius, radius]
                let mapWidth = 2*radius + 1;
                let map = Array(mapWidth).fill().map(() => Array(mapWidth).fill(0));
                for (let i = 0; i < activePiece.tileDeltas.length; ++i) {
                    let coords = vec.addvec([radius, radius], activePiece.tileDeltas[i]);
                    map[coords[0]][coords[1]] = 1;
                }
                for (let i = 0; i < activePiece.tileDeltas.length; ++i) {
                    // get a 3x3 map around the tile
                    let localMap = vec.slicem(
                        map,
                        vec.addvec(activePiece.tileDeltas[i], [-1,-1], [radius, radius]),
                        vec.addvec(activePiece.tileDeltas[i], [1,1], [radius, radius])
                    );

                    let tileInfo = getFirstMatchingSpriteTile(localMap);
                    activePiece.tileSprites.push(tileInfo.coord);
                }
            };

            /**
             * Creates a new piece object.
             * @param {Number=} pieceId - The piece id to create. 
             * @returns {ActivePiece} A new piece object. 
             */
            let rollNewPiece = function(pieceId) {
                if (pieceId == null) {
                    pieceId = Math.floor(Math.random() * tetrominos.pieceTypes.length);
                }
                return {
                    pieceType: {    // copy pieceType information
                        states: tetrominos.pieceTypes[pieceId].states,
                        name: tetrominos.pieceTypes[pieceId].name,
                        originalTileDeltas: [...tetrominos.pieceTypes[pieceId].tiles],
                        id: pieceId
                    },
                    position: [tetrominos.info.maxDistance.left, tetrominos.info.maxDistance.down],
                    tileDeltas: [...tetrominos.pieceTypes[pieceId].tiles],
                    rotationState: 0,
                    tileSprites: [],
                }
            };

            /**
             * Adds the upcoming piece to the board and rolls a new upcoming piece.
             * If there is no upcoming piece, will automatically generate a new one.
             * @param {GameState} gameState - The current game state.
             */
            let spawnNewPiece = function(gameState) {
                if (!gameState.upcomingPiece.pieceType) {
                    gameState.upcomingPiece = rollNewPiece();
                    updateSpriteTable(gameState.upcomingPiece);
                }
                gameState.activePiece = gameState.upcomingPiece;
                boardTools.removePieceFromBoard(gameState.upcomingPiece, gameState.preview);

                // fill in extra data
                gameState.activePiece.position = opts.newPiecePos.slice();
                gameState.activePiece.lastFall = gameState.currentFrame;
                gameState.activePiece.lastRotation = gameState.currentFrame;
                gameState.activePiece.lastShift = gameState.currentFrame;

                gameState.upcomingPiece = rollNewPiece();
                updateSpriteTable(gameState.upcomingPiece);

                boardTools.addPieceToBoard(gameState.activePiece, gameState.board);
                boardTools.addPieceToBoard(gameState.upcomingPiece, gameState.preview);
                gameState.lastPieceCreation = gameState.currentFrame;
            };

            /**
             * Move the active piece down one square.
             * @param {!GameState} gameState 
             */
            let dropPiece = function(gameState) {
                boardTools.removePieceFromBoard(gameState.activePiece, gameState.board);
                gameState.activePiece.position = vec.addvec(gameState.activePiece.position, down);
                boardTools.addPieceToBoard(gameState.activePiece, gameState.board);
                gameState.activePiece.lastFall = gameState.currentFrame;
            };

            /**
             * Removes a row from the board and spawns a new row on top.
             * @param {GameState} gameState - The current game state.
             * @param {Number} y - The row number to collapse.
             */
            let collapseRow = function(gameState, y) {
                gameState.board.tiles = gameState.board.tiles.map(
                    (x) => x.slice(0, y).concat(x.slice(y+1, x.length)).concat([boardTools.getNewEmptyTile()])
                );
            };

            /**
             * Adds the active piece to the board as a fallen piece/
             * @param {GameState} gameState - The current game state.
             * @returns {Number[]} A list of y-coordinates that had a block added to them. 
             */
            let solidifyActivePiece = function(gameState) {
                let yCoords = [];
                for (let i = 0; i < gameState.activePiece.tileDeltas.length; ++i) {
                    let position = vec.addvec(gameState.activePiece.position, gameState.activePiece.tileDeltas[i]);
                    if (gameState.board.tiles[position[0]] && gameState.board.tiles[position[0]][position[1]]) {
                        gameState.board.tiles[position[0]][position[1]].filled = true;
                        yCoords.push(position[1]);
                    }
                }
                gameState.activePiece = {};
                return yCoords.filter((elmt, index, array) => array.indexOf(elmt) === index).sort((a, b) => a-b);
            };

            let handleCollision = function(gameState) {
                let yCoords = solidifyActivePiece(gameState);
                let rowsFilled = 0;
                gameState.animation = animationTools.getNewAnimation(opts.pieceDroppedPauseLength);
                while (yCoords.length) {
                    if (boardTools.checkRowFill(gameState.board, yCoords[yCoords.length-1])) {
                        gameState.animation.totalDuration = opts.rowCompletedAnimationLength;
                        gameState.animation.rows.push(yCoords[yCoords.length-1]);
                        ++rowsFilled;
                    }
                    yCoords.pop();
                }
                addStats(gameState, rowsFilled);
            };

            let rotatePiece = function(gameState) {
                // rotates the active piece counterclockwise
                if (gameState.activePiece.pieceType.states > 1) {
                    let newDeltas;
                    if (gameState.activePiece.rotationState >= gameState.activePiece.pieceType.states - 1) {
                        newDeltas = [...gameState.activePiece.pieceType.originalTileDeltas];
                    } else {
                        newDeltas = gameState.activePiece.tileDeltas.map((x) => vec.multm22v2(pieceRotateMatrix, x));
                    }
                    if (!boardTools.checkCollision(gameState.board, newDeltas, gameState.activePiece.position)) {
                        boardTools.removePieceFromBoard(gameState.activePiece, gameState.board);
                        gameState.activePiece.tileDeltas = newDeltas;
                        updateSpriteTable(gameState.activePiece);
                        gameState.activePiece.lastRotation = gameState.currentFrame;
                        boardTools.addPieceToBoard(gameState.activePiece, gameState.board);
                        gameState.activePiece.rotationState = (
                            (gameState.activePiece.rotationState + 1)
                             % gameState.activePiece.pieceType.states);
                    }
                }
            };
            let movePieceRight = function(gameState) {
                if(boardTools.checkCollision(
                        gameState.board, 
                        gameState.activePiece.tileDeltas,
                        vec.addvec(gameState.activePiece.position, right)
                )) {
                    return false;
                } else {
                    boardTools.removePieceFromBoard(gameState.activePiece, gameState.board);
                    gameState.activePiece.position = vec.addvec(gameState.activePiece.position, right);
                    boardTools.addPieceToBoard(gameState.activePiece, gameState.board);
                    gameState.activePiece.lastShift = gameState.currentFrame;
                    return true;
                }
            };
            let movePieceLeft = function(gameState) {
                if(boardTools.checkCollision(
                    gameState.board,
                    gameState.activePiece.tileDeltas,
                    vec.addvec(gameState.activePiece.position, left)
                )) {
                    return false;
                } else {
                    boardTools.removePieceFromBoard(gameState.activePiece, gameState.board);
                    gameState.activePiece.position = vec.addvec(gameState.activePiece.position, left);
                    boardTools.addPieceToBoard(gameState.activePiece, gameState.board);
                    gameState.activePiece.lastShift = gameState.currentFrame;
                    return true;
                }
            };
            let hardDropPiece = function(gameState) {
                boardTools.removePieceFromBoard(gameState.activePiece, gameState.board);
                let tilesDown = 0;
                while (!boardTools.checkCollision(
                    gameState.board,
                    gameState.activePiece.tileDeltas,
                    vec.addvec(gameState.activePiece.position, [0, tilesDown])
                )) {
                    --tilesDown;
                }
                gameState.activePiece.position[1] += tilesDown + 1;
                boardTools.addPieceToBoard(gameState.activePiece, gameState.board);
                handleCollision(gameState);
                return true;
            }

            let gameLogic = {
                /**
                 * Increments the game one time step forward (outside of menus).
                 * @param {!AppState} state - The current game state
                 */
                tick: function(state) {
                    let gameState = state.gameState || (state.gameState = this.getNewGameState());
                    ++gameState.currentFrame;
                    
                    // quick restart
                    if (
                        options.inputUtils.checkCommand(options.keys.RESTART, state.pressedKeys)
                    ) {
                        state.gameState = this.getNewGameState();
                        return;
                    }
                    // quick quit
                    if (
                        options.inputUtils.checkCommand(options.keys.QUIT, state.pressedKeys)
                    ) {
                        state.gameState = false;
                        return;
                    }

                    // If there is an animation, tick that and pause the rest of the game logic.
                    if (gameState.animation) {
                        if (animationTools.tick(gameState.animation, gameState)) {
                            return;
                        } else {
                            for (let i = 0; i < gameState.animation.rows.length; ++i) {
                                collapseRow(gameState, gameState.animation.rows[i]);
                            }
                            gameState.animation = false;
                        }
                    }
                    
                    // Make sure there is always a piece ready to go.
                    if (!gameState.activePiece.pieceType) {
                        spawnNewPiece(gameState);
                        if (boardTools.checkCollision(
                            gameState.board,
                            gameState.activePiece.tileDeltas,
                            gameState.activePiece.position
                        )) {
                            this.fail(gameState);
                            return false;
                        }
                    }

                    // soft and hard drop
                    let curFallingSpeed = gameState.fallingSpeed;
                    if (options.inputUtils.checkCommand(options.keys.SOFT_DROP, state.pressedKeys)) {
                        curFallingSpeed = opts.softDropSpeed;
                    }
                    if (
                        options.inputUtils.checkCommand(options.keys.HARD_DROP, state.pressedKeys)
                        && gameState.currentFrame > gameState.lastPieceCreation + opts.hardDropDelay
                    ) {
                        hardDropPiece(gameState);
                    }

                    // move left or right
                    if (
                        options.inputUtils.checkCommand(options.keys.LEFT, state.pressedKeys)
                        && gameState.currentFrame > gameState.activePiece.lastShift + opts.shiftDelay
                    ) {
                        movePieceLeft(gameState);
                    }
                    if (
                        options.inputUtils.checkCommand(options.keys.RIGHT, state.pressedKeys)
                        && gameState.currentFrame > gameState.activePiece.lastShift + opts.shiftDelay
                    ) {
                        movePieceRight(gameState);
                    }
                    // rotate
                    if (
                        options.inputUtils.checkCommand(options.keys.ROTATE, state.pressedKeys)
                        && gameState.currentFrame > gameState.activePiece.lastRotation + opts.rotateDelay
                    ) {
                        rotatePiece(gameState);
                    }

                    // check for collisions
                    if (gameState.currentFrame >= gameState.activePiece.lastFall + curFallingSpeed) {
                        if (boardTools.checkCollision(
                            gameState.board,
                            gameState.activePiece.tileDeltas,
                            vec.addvec(gameState.activePiece.position, down)
                        )) {
                            handleCollision(gameState);
                        } else {
                            dropPiece(gameState);
                        }
                    }
                    log.tick(gameState);
                },

                /**
                 * Return a new gameState object.
                 * @returns {!GameState} A clean game state.
                 */
                getNewGameState: function() {
                    let gameState = {
                        board: boardTools.getNewBoard(options.boardWidth, options.boardHeight),
                        activePiece: {},
                        preview: boardTools.getNewBoard(
                            tetrominos.info.maxDistance.left + tetrominos.info.maxDistance.right + 1,
                            tetrominos.info.maxDistance.down + tetrominos.info.maxDistance.up + 1),
                        upcomingPiece: {},
                        fallingSpeed: opts.startingSpeed,
                        currentFrame: 0,
                        lastPieceCreation: 0,
                        gameOver: false,
                        stats: getNewStats(),
                        log: log.getNew(),
                        highScore: false
                    };
                    gameState.board.options = displayUtils.getDisplayOptions("board");
                    gameState.preview.options = displayUtils.getDisplayOptions("preview");
                    return gameState;
                },

                fail: function(gameState) {
                    gameState.gameOver = true;
                    if (options.highScoreUtils.isHighScore(gameState.stats.score)) {
                        gameState.highScore = true;
                    }
                },

                boardTools: boardTools,
            }

            resolve(gameLogic);
        }).catch((error) => console.error(error));
    }).catch((error) => console.error(error));
})(window.setup = window.setup || {});

