"use strict";

(function main() {
    // start webgl
    const gl = canvas.getContext("webgl");

    if (!gl) {
        alert("Failed to start webgl");
        return;
    }

    // setup textures    
    let createTexture = function(image) {
        // create a texture object
        let texture = gl.createTexture();
        gl.bindTexture(gl.TEXTURE_2D, texture);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);

        // Now that the image has loaded, copy it to texture
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA,gl.UNSIGNED_BYTE, image);
        return texture;
    };

    let texturesSetup = new Promise((resolve) => {
        Promise.all([
            resources.loadTileSheet,
            resources.loadPalette,
        ]).then((inputs) => {
            let [tileSheetImage, paletteImage] = inputs;

            // setup tile sheet texture
            gl.activeTexture(gl.TEXTURE0);
            let tileSheetTexture = createTexture(tileSheetImage);

            // setup palette
            gl.activeTexture(gl.TEXTURE1);
            let paletteTexture = createTexture(paletteImage);

            resolve(tileSheetTexture, paletteTexture);
        }).catch((error) => console.error(error));
    }).catch((error) => console.error(error));

    // setup shader program
    var shaderProgramSetup = new Promise((resolve) => {
        Promise.all([
            resources.loadVertShader,
            resources.loadFragShader
        ]).then((shaders) => {
            var program = gl.createProgram();
            glUtils.compileProgram(gl, program, shaders[0], shaders[1]);
            gl.useProgram(program);
            resolve(program);
        });
    });

    // after all setup is done
    Promise.all([
        shaderProgramSetup,
        setup.appLogicSetup,
        texturesSetup
    ]).then((inputs) => {
        var [program, app] = inputs;

        // get shader variable locations
        var positionLoc = gl.getAttribLocation(program, "a_Position");
        var texCoordLoc = gl.getAttribLocation(program, "a_TexCoord");
        var paletteIndexLoc = gl.getAttribLocation(program, "a_PaletteIndex");
        var fadeFactorLoc = gl.getAttribLocation(program, "a_FadeFactor");
        var uTilesheetLoc = gl.getUniformLocation(program, "u_Tilesheet");
        var uPaletteLoc = gl.getUniformLocation(program, "u_Palette");
        gl.enableVertexAttribArray(positionLoc);
        gl.enableVertexAttribArray(texCoordLoc);
        gl.enableVertexAttribArray(paletteIndexLoc);
        gl.enableVertexAttribArray(fadeFactorLoc);

        // render variables
        var vertex_buffer = gl.createBuffer();
        var texCoord_buffer = gl.createBuffer();
        var paletteIndex_buffer = gl.createBuffer();
        var fadeFactor_buffer = gl.createBuffer();

        // flags
        gl.enable(gl.DEPTH_TEST);

        // begin the render loop
        render(0);

        function render(now) {
            let renderData = app.tick(now);

            // buffer data
            gl.uniform1i(uTilesheetLoc, 0);
            gl.uniform1i(uPaletteLoc, 1);

            gl.bindBuffer(gl.ARRAY_BUFFER, vertex_buffer);
            gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(renderData.vertices), gl.STATIC_DRAW);
            // location, size, type, normalize, stride, offset
            gl.vertexAttribPointer(positionLoc, 2, gl.FLOAT, false, 0, 0);
            
            gl.bindBuffer(gl.ARRAY_BUFFER, texCoord_buffer);
            gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(renderData.texCoords), gl.STATIC_DRAW);
            gl.vertexAttribPointer(texCoordLoc, 2, gl.FLOAT, false, 0, 0);

            gl.bindBuffer(gl.ARRAY_BUFFER, paletteIndex_buffer);
            gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(renderData.paletteIndices), gl.STATIC_DRAW);
            gl.vertexAttribPointer(paletteIndexLoc, 1, gl.FLOAT, false, 0, 0);

            gl.bindBuffer(gl.ARRAY_BUFFER, fadeFactor_buffer);
            gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(renderData.fadeFactors), gl.STATIC_DRAW);
            gl.vertexAttribPointer(fadeFactorLoc, 1, gl.FLOAT, false, 0, 0);
            
            // actually draw
            gl.clearColor(0.0, 0.0, 0.0, 1.0);
            gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
            gl.viewport(0, 0, canvas.width, canvas.height);
            
            gl.drawArrays(gl.TRIANGLES, 0, renderData.numTriangles*3);
            
            window.requestAnimationFrame(render);
        }
    }).catch((error) => {console.error(error)});
})();

