"use strict";

(function(setup) {
    setup.menuLogicSetup = new Promise((resolve) => {
        Promise.all([
            resources.loadTileSheetInfo,
            resources.loadTetrominos,
            setup.optionsSetup,
            setup.displayUtilsSetup,
            setup.gameLogicSetup
        ]).then((inputs) => {
            let [tileSheet, tetrominos, options, displayUtils, game] = inputs;
            
            /**
             * Safely leaves a menu, cancelling any text input fields if necessary.
             * @param {!AppState} - The current state of the application.
             */
            let leaveMenu = function(state) {
                if (state.menuState.textInputIndex != null) {
                    // clear information from the stack and mark the entry to be ignored
                    state.textInputStack.text[state.menuState.textInputIndex] = "";
                    state.textInputStack.maxLength[state.menuState.textInputIndex] = 0;
                }
                state.menuState = false;
            };

            // buttons
            let newGameButton = {
                displayText: "New Game",
                onSelect: "newGame"
            };
            let continueGameButton = {
                displayText: "Continue",
                onSelect: "continueGame"
            };
            let pieceViewButton = {
                displayText: "Pieces",
                onSelect: "pieceView"
            };
            let mainMenuButton = {
                displayText: "Quit",
                onSelect: "mainMenu"
            };
            let viewHighScoresButton = {
                displayText: "High Scores",
                onSelect: "viewHighScores"
            };
            let backToMainMenuButton = {
                displayText: "Back",
                onSelect: "mainMenu"
            };
            let clearHighScoresButton = {
                displayText: "Reset Scores",
                onSelect: "resetHighscores"
            };
            let paletteSwapMenuButton = {
                displayText: "Palettes",
                onSelect: "piecePaletteMenu"
            };
            let paletteSwapPieceButton = {
                displayText: "",
                onSelect: "incrementPiecePalette"
            };
            let resetPiecePalettesButton = {
                displayText: "Reset",
                onSelect: "resetPiecePalette"
            };

            let newMenu = {
                /**
                 * Gets a new main menu state.
                 * @param {!Number} now - The current time from window.requestAnimationFrame.
                 * @returns {MenuState} The new menuState.
                 */
                mainMenu: function(now) {
                    let menuState = {
                        titleCard: {
                            options: displayUtils.getDisplayOptions("titleCard"),
                            tileIndex: [
                                tileSheet.tileMap.TITLECARD.coord,
                                tileSheet.tileMap.TITLECARD.size],
                            lastColourChange: now
                        },
                        header: {
                            options: displayUtils.getDisplayOptions("mainMenu"),
                        },
                        menuItems: [
                            newGameButton,
                            viewHighScoresButton,
                            paletteSwapMenuButton
                        ],
                        log: {
                            text: "@ 2019 Matthew Marinets.",
                            options: displayUtils.getDisplayOptions("copyrightNotice")
                        },
                        cursorPosition: 0,
                        lastCursorMove: now,
                        lastMenuSelect: now,
                        menuType: "full",
                        tickType: "titleCard"
                    };
                    menuState.titleCard.options.palette = Math.floor(Math.random() * 8)
                    return menuState;
                },

                /**
                 * Return a menuState object for the main menu.
                 * @param {!Number} now - The current time from window.requestAnimationFrame.
                 * @param {Object[][]=} board - The current gameState's gameboard.
                 * @returns {!MenuState} A clean menu state.
                 */
                pauseMenu: function(now, board) {
                    let menuState = {
                        header: {
                            options: displayUtils.getDisplayOptions("sideMenu"),
                            title: "Paused",
                        },
                        menuItems: [
                            newGameButton,
                            continueGameButton,
                            mainMenuButton,
                            pieceViewButton
                        ],
                        cursorPosition: 1,
                        lastCursorMove: now,
                        lastMenuSelect: now,
                        menuType: "side"
                    };
                    if (board) {
                        menuState.board = getBackgroundBoard(board);
                    }
                    return menuState;
                },

                /**
                 * Return a menuState object for a game over.
                 * @param {!AppState} state - The current application state.
                 * @returns {!MenuState} A clean menu state.
                 */
                gameOver: function(now, state) {
                    let gameState = state.gameState;
                    if (gameState.highScore) {
                        return this.highScoreNamePrompt(state, now)
                    }
                    let menuState = {
                        header: {
                            options: displayUtils.getDisplayOptions("sideMenu"),
                            title: ["Game", "Over"],
                            subtitle: "Score: ".concat(gameState.stats.score)
                        },
                        menuItems: [
                            newGameButton,
                            mainMenuButton,
                            pieceViewButton
                        ],
                        cursorPosition: 0,
                        lastCursorMove: now,
                        lastMenuSelect: now,
                        menuType: "side"
                    };
                    menuState.board = getBackgroundBoard(gameState.board);
                    menuState.header.options.title.palette = 0;
                    return menuState;
                },

                paletteSwap: function(now) {
                    let menuState = {
                        header: {
                            options: displayUtils.getDisplayOptions("paletteSwapMenu"),
                            title: "Palettes"
                        },
                        menuItems: tetrominos.pieceTypes.map(
                                () => paletteSwapPieceButton
                            ).concat([resetPiecePalettesButton, backToMainMenuButton]),
                        preview: game.boardTools.getPalettePreviewBoard(),
                        cursorPosition: 0,
                        lastCursorMove: now,
                        lastMenuSelect: now,
                        lastPaletteChange: now,
                        menuType: "full",
                        tickType: "paletteSwap"
                    };
                    displayUtils.formatRenderOptions(menuState.header.options.title);
                    displayUtils.formatRenderOptions(menuState.header.options.subtitle);
                    displayUtils.formatRenderOptions(menuState.header.options.menuItems);
                    menuState.preview.options = {
                        position: [
                            menuState.header.options.position[0]
                            + menuState.header.options.menuItems.fontSize.dx * 2,
                            menuState.header.options.position[1]
                            - menuState.header.options.title.fontSize.dy
                            - menuState.header.options.subtitle.fontSize.dy
                        ],
                        fontSize: {
                            mode: menuState.header.options.menuItems.fontSize.mode,
                            w: menuState.header.options.menuItems.fontSize.w,
                            h: menuState.header.options.menuItems.fontSize.h,
                            dx: menuState.header.options.menuItems.fontSize.w,
                            dy: menuState.header.options.menuItems.fontSize.h
                        }
                    };
                    return menuState;
                },

                /**
                 * Gets a new leaderboard menu state.
                 * @param {!Number} now - The current time from window.requestAnimationFrame.
                 * @returns {MenuState} The new menuState.
                 */
                leaderboard: function(now) {
                    let menuState = {
                        header: {
                            options: displayUtils.getDisplayOptions("leaderboard"),
                            title: "Leaderboard",
                            body: []
                        },
                        menuItems: [
                            clearHighScoresButton,
                            backToMainMenuButton
                        ],
                        cursorPosition: 1,
                        lastCursorMove: now,
                        lastMenuSelect: now,
                        menuType: "full"
                    };
                    // ensure body palettes are an empty list
                    menuState.header.options.body.palette = [];
                    let highScores = options.highScoreUtils.getHighScores();
                    for (let i = 0; i < highScores.length; ++i) {
                        if (highScores[i].score === 0) {
                            menuState.header.body.push("+");
                            menuState.header.options.body.palette.push(15);
                        } else {
                            menuState.header.body.push(
                                highScores[i].name.padEnd(options.menu.maximumHighScoreNameLength, " ")
                                .concat(highScores[i].score.toString().padStart(25 - options.menu.maximumHighScoreNameLength, " "))
                            );
                            menuState.header.options.body.palette.push(highScores[i].colour);
                        }
                    }
                    return menuState;
                },

                /**
                 * Gets a new high score menu state.
                 * @param {!AppState} state - The global state of the application.
                 * @param {!Number} now - The current time from window.requestAnimationFrame.
                 * @returns {MenuState} The new menuState.
                 */
                highScoreNamePrompt: function(state, now) {
                    let menuState = {
                        header: {
                            options: displayUtils.getDisplayOptions("highScoreNamePrompt"),
                            title: ["High Score!"],
                            subtitle: state.gameState.stats.score.toString().padStart(
                                7 + Math.floor(state.gameState.stats.score.toString().length)/2, " "),
                            body: ["  Enter Name:", ""]
                        },
                        textInputIndex: 0,
                        lastMenuSelect: now,
                        lastPaletteChange: now,
                        menuType: "full",
                        tickType: "highScore"
                    };
                    // Create a new focus for the text input stack and store the index into the stack.
                    state.textInputStack.maxLength.push(options.menu.maximumHighScoreNameLength);
                    state.textInputStack.text.push("");
                    menuState.textInputIndex = state.textInputStack.text.length - 1;

                    // ensure that the body's palette option exists
                    menuState.header.options.body.palette = [15, menuState.header.options.body.palette || 15]
                    return menuState;
                },
            };


            /**
             * Returns a reformatted version of the input board to be displayed as a background board.
             * @param {GameBoard} board - The current gameboard. 
             */
            let getBackgroundBoard = function(board) {
                return {
                    tiles: board.tiles.slice(),
                    options: displayUtils.getDisplayOptions("backgroundBoard")
                }
            };

            let buttonFunctions = {
                newGame: (state) => {
                    state.gameState = game.getNewGameState();
                    leaveMenu(state);
                },
                continueGame: (state) => leaveMenu(state),
                pieceView: (state) => {
                    if (state.menuState.preview) {
                        // remove piece preview if it exists.
                        state.menuState.preview = false;
                        if (state.menuState.menuType === "side" && state.gameState.board) {
                            // restore game preview
                            state.menuState.board = getBackgroundBoard(state.gameState.board);
                            state.menuState.preview = false;
                        }
                    } else if (state.menuState.menuType === "side" && state.menuState.board) {
                        // add side piece display
                        state.menuState.board = false;
                        state.menuState.preview = game.boardTools.getPiecePreviewBoard(13, 27);
                        state.menuState.preview.options = displayUtils.getDisplayOptions("pieceDisplayLeft");
                    } else {
                        // add bottom piece display
                        state.menuState.preview = game.boardTools.getPiecePreviewBoard(22, 14);
                        state.menuState.preview.options = displayUtils.getDisplayOptions("pieceDisplayBottom");
                    }
                },
                mainMenu: (state, now) => {
                    leaveMenu(state);
                    state.menuState = newMenu.mainMenu(now);
                    state.gameState = false;
                },
                viewHighScores: (state, now) => {
                    leaveMenu(state);
                    state.menuState = newMenu.leaderboard(now);
                },
                resetHighscores: (state, now) => {
                    options.highScoreUtils.clearHighScores();
                    state.menuState = newMenu.leaderboard(now);
                },
                piecePaletteMenu: (state, now) => {
                    state.menuState = newMenu.paletteSwap(now);
                },
                incrementPiecePalette: (state, now, cursorPos) => {
                    options.colourAssignmentUtils.updateColourAssignment(
                        cursorPos,
                        (options.colourAssignment[cursorPos] + 1) % 15
                    );
                    state.menuState.preview.tiles = game.boardTools.getPalettePreviewBoard().tiles;
                },
                resetPiecePalette: (state) => {
                    options.colourAssignmentUtils.resetColourAssignment();
                    state.menuState.preview.tiles = game.boardTools.getPalettePreviewBoard().tiles;
                },
            };

            let tickLogic = {
                /**
                 * Ticks the highscore name input prompt for one time increment.
                 * @param {!AppState} state - The global state of the application.
                 * @param {!Number} now - The current time from window.requestAnimationFrame.
                 */
                highScore: function(state, now) {
                    let menuState = state.menuState;
                    // Copy the current text input contents to the body text field for display.
                    menuState.header.body[1] = state.textInputStack.text[menuState.textInputIndex];

                    // only do things if there is no text input prompt of higher priority.
                    if (state.textInputStack.text.length === menuState.textInputIndex + 1) {
                        // submit name
                        if (
                            options.inputUtils.checkCommand(options.keys.MENU_SELECT, state.pressedKeys)
                            && now - menuState.lastMenuSelect > options.menu.selectionRefreshSpeed
                        ) {
                            menuState.lastMenuSelect = now;
                            let name = state.textInputStack.text.pop();
                            state.textInputStack.maxLength.pop();
                            options.highScoreUtils.submitHighScore(name, state.gameState.stats.score, menuState.header.options.body.palette[1]);
                            leaveMenu(state);
                            state.menuState = newMenu.mainMenu(now);
                        }
                        // change colour palette
                        if (
                            options.inputUtils.checkCommand(options.keys.MENU_UP, state.pressedKeys)
                            && now - menuState.lastPaletteChange > options.menu.cursorMoveRefreshSpeed
                        ) {
                            menuState.lastPaletteChange = now;
                            menuState.header.options.body.palette[1] = (menuState.header.options.body.palette[1]+1) % 16;
                        }
                        if (
                            options.inputUtils.checkCommand(options.keys.MENU_DOWN, state.pressedKeys)
                            && now - menuState.lastPaletteChange > options.menu.cursorMoveRefreshSpeed
                        ) {
                            menuState.lastPaletteChange = now;
                            // javascript % is not a proper modulo - it gives negative output for negative input.
                            menuState.header.options.body.palette[1] = (((menuState.header.options.body.palette[1]-1) % 16) + 16) % 16;
                        }
                    }
                },

                titleCard: function(state, now) {
                    // Change the colour of the title card periodically.
                    let menuState = state.menuState;
                    if (now - menuState.titleCard.lastColourChange > options.menu.titleCardColourChangeTime) {
                        menuState.titleCard.options.palette = 
                            (menuState.titleCard.options.palette + Math.floor(Math.random() * 14)) % 15;
                        menuState.titleCard.lastColourChange = now;
                    }
                },

                paletteSwap: function(state, now) {
                    // use side arrow keys to change palettes forward and back
                    let menuState = state.menuState;
                    if (options.inputUtils.checkCommand(options.keys.RIGHT, state.pressedKeys)
                        && now - menuState.lastPaletteChange > options.menu.paletteSwapButtonRefreshSpeed
                    ) {
                        options.colourAssignmentUtils.updateColourAssignment(
                            menuState.cursorPosition,
                            (options.colourAssignment[menuState.cursorPosition] + 1) % 15
                        );
                        state.menuState.preview.tiles = game.boardTools.getPalettePreviewBoard().tiles;
                        menuState.lastPaletteChange = now;
                    }
                    if (options.inputUtils.checkCommand(options.keys.LEFT, state.pressedKeys)
                        && now - menuState.lastPaletteChange > options.menu.paletteSwapButtonRefreshSpeed
                    ) {
                        options.colourAssignmentUtils.updateColourAssignment(
                            menuState.cursorPosition,
                            ((options.colourAssignment[menuState.cursorPosition] - 1) % 15 + 15) % 15 
                        );
                        state.menuState.preview.tiles = game.boardTools.getPalettePreviewBoard().tiles;
                        menuState.lastPaletteChange = now;
                    }
                }
            }
            
            let menuLogic = {
                // make these public;
                // Declared at the top for use within this module.
                newMenu: newMenu,
                leaveMenu: leaveMenu,

                /**
                 * Ticks the menu for one frame.
                 * @param {AppState} state - The current state of the application.
                 * @param {Number} now - The current time gotten from window.requestAnimationFrame.
                 */
                tick: function(state, now) {
                    let menuState = state.menuState;

                    // perform special menu-specific ticks
                    if (menuState.tickType) {
                        tickLogic[menuState.tickType](state, now);
                    }
                    
                    // Move cursor up.
                    if (
                        options.inputUtils.checkCommand(options.keys.MENU_UP, state.pressedKeys)
                        && menuState.cursorPosition != null
                        && (now - menuState.lastCursorMove > options.menu.cursorMoveRefreshSpeed)
                    ) {
                        // javascript % isn't a true modulo opterator - hence the complicated statement.
                        menuState.cursorPosition = (
                            (menuState.cursorPosition - 1) % menuState.menuItems.length
                             + menuState.menuItems.length
                            ) % menuState.menuItems.length;
                        menuState.lastCursorMove = now;
                    }
                    // Move cursor down.
                    if (
                        options.inputUtils.checkCommand(options.keys.MENU_DOWN, state.pressedKeys)
                        && menuState.cursorPosition != null
                        && (now - menuState.lastCursorMove > options.menu.cursorMoveRefreshSpeed)
                    ) {
                        menuState.cursorPosition = (menuState.cursorPosition + 1) % menuState.menuItems.length;
                        menuState.lastCursorMove = now;
                    }
                    // select from the menu
                    if (
                        options.inputUtils.checkCommand(options.keys.MENU_SELECT, state.pressedKeys)
                        && now - menuState.lastMenuSelect > options.menu.selectionRefreshSpeed
                    ) {
                        buttonFunctions[menuState.menuItems[menuState.cursorPosition].onSelect](state, now, menuState.cursorPosition);
                        menuState.lastMenuSelect = now;
                    }

                    // quick restart
                    if (
                        options.inputUtils.checkCommand(options.keys.RESTART, state.pressedKeys)
                        && menuState.textInputIndex == null
                        && now - menuState.lastMenuSelect > options.menu.selectionRefreshSpeed
                    ) {
                        buttonFunctions.newGame(state, now);
                    }
                }
            };
            resolve(menuLogic);
        }).catch((error) => console.error(error));
    }).catch((error) => console.error(error));
})(window.setup = window.setup || {});