"use strict";

/**
 * Sets up all options.
 */
(function(setup) {
    setup.optionsSetup = new Promise((resolve) => {
        Promise.all([
            resources.loadTileSheetInfo,
            resources.loadTetrominos,
        ]).then((inputs) => {
            let [tileSheet, tetrominos] = inputs;
            let options = {};

            // gameplay options
            options.boardHeight = 20,
            options.boardWidth = 10,
            options.unassignedPalette = -1;
            options.game = {
                startingSpeed: 20,   // the number of frames it takes for the piece to fall one tile
                maxFallSpeed: 6,
                softDropSpeed: 2,       // the speed when the down key is held
                shiftDelay: 5,       // minimum number of frames between sideways move
                rotateDelay: 8,      // minimum number of frames between rotations
                hardDropDelay: 10,
                pointsPerPiece: 0,
                pointsPerRow: [0, 40, 100, 300, 1200],  // NES style
                // pointsPerRow: [0, 100, 300, 500, 800],  // new style
                pointsToFirstLevel: 20,
                newPiecePos: [Math.floor(options.boardWidth / 2) - 1, Math.floor(options.boardHeight) - 1],
                messages: [false, "Row complete!", "Double :D", "~Triple~", "**TETRIS**"],
                messageColours: ["Rainbow7", 3, 4, 5, "Rainbow7"],
                emptyTileSprite: [5, 4],
                logDisplayTime: 150,
                rowCompletedAnimationLength: 20,
                pieceDroppedPauseLength: 5
            };

            // menu options
            options.menu = {
                menuToggleRefreshSpeed: 150, 
                cursorMoveRefreshSpeed: 150, 
                selectionRefreshSpeed: 300,
                paletteSwapButtonRefreshSpeed: 140,
                saveStateRefreshSpeed: 500,
                maximumHighScoreNameLength: 15,
                numHighScores: 10,
                titleCardColourChangeTime: 2000
            };

            // keybindings
            options.keys = {
                LEFT: ["ArrowLeft", "a", "j"],
                RIGHT: ["ArrowRight", "d", "l"],
                UP: ["ArrowUp", "w", "i"],
                DOWN: ["ArrowDown", "s", "k"],
                SOFT_DROP: ["ArrowDown", "s", "k"],
                HARD_DROP: [" "],
                ROTATE: ["ArrowUp", "w", "i"],
                MENU: ["Escape"],
                MENU_SELECT: ["Enter"],
                MENU_UP: ["ArrowUp"],
                MENU_DOWN: ["ArrowDown"],
                QUIT: ["q"],
                RESTART: ["r"],
                SAVE_SLOT_1: ["F1"],
                LOAD_SLOT_1: ["F2"],
            };

            // layout measurements
            // canvas
            let boardPixel = {
                w: 2 / canvas.width,
                h: 2 / canvas.height
            };
            options.canvasSizes = {
                pixel: boardPixel,
                tile: {
                    w: 128 * boardPixel.w,
                    h: 128 * boardPixel.h,
                    dx: 127 * boardPixel.w,
                    dy: 127 * boardPixel.h
                },
                board: {}
            };
            options.canvasSizes.board = {
                w: options.canvasSizes.tile.dx * (options.boardWidth - 1) + options.canvasSizes.tile.w,
                h: options.canvasSizes.tile.dy * (options.boardHeight - 1) + options.canvasSizes.tile.h
            };

            // tilesheet
            let spritePixel = {
                w: 1 / tileSheet.sheet.w,
                h: 1 / tileSheet.sheet.h
            };
            options.sprite = {
                pixel: spritePixel,
                tile: {
                    w: spritePixel.w * tileSheet.tile.w,
                    h: spritePixel.h * tileSheet.tile.w,
                    adjustTop: [0.0, spritePixel.h / 4],
                    adjustLeft: [spritePixel.w / 4, 0.0],
                    adjustRight: [-spritePixel.w / 4, 0.0],
                    adjustBottom: [0.0, -spritePixel.h / 4]
                }
            };
            
            // palettesheet
            options.normalizedPalette = {
                w: 1.0,
                h: 1.0,
                pixel: {
                    w: 1.0 / 256.0,
                    h: 1.0 / 16.0
                }
            };

            // Tetromino palette assignment manipulation.
            let colourAssignmentKey = "colourAssignments";

            // ensure the colour assignment exists and is formatted correctly.
            let colourAssignment;
            try {
                colourAssignment = JSON.parse(window.localStorage.getItem(colourAssignmentKey));
            } catch(e) {
                colourAssignment = false;
            }
            colourAssignment = colourAssignment || JSON.parse(JSON.stringify(tetrominos.info.defaultPalettes));
            while (colourAssignment.length < tetrominos.pieceTypes.length) {
                colourAssignment.push(tetrominos.info.defaultPalettes[colourAssignment.length] || 0)
            }
            while (colourAssignment.length > tetrominos.pieceTypes.length) {
                colourAssignment.pop();
            }
            options.colourAssignment = colourAssignment;
            options.colourAssignmentUtils = {
                /**
                 * Updates a piece
                 * @param {Number} index - The piece index whose palette is to be adjusted.
                 * @param {*} colour - The colour to set the palette to.
                 */
                updateColourAssignment: function(index, colour) {
                    options.colourAssignment[index] = colour;
                    window.localStorage.setItem(colourAssignmentKey, JSON.stringify(options.colourAssignment));
                },
                /**
                 * Resets the colour assignments to default.
                 */
                resetColourAssignment: function() {
                    options.colourAssignment = [];
                    for (let i = 0; i < tetrominos.pieceTypes.length; ++i) {
                        options.colourAssignment.push(tetrominos.info.defaultPalettes[i] || 0)
                    }
                    window.localStorage.setItem(colourAssignmentKey, JSON.stringify(options.colourAssignment));
                }
            }
            

            // Utilities for checking keyboard inputs.
            options.inputUtils = {
                /**
                 * Checks whether a certain command is being executed
                 * @param {String} command The command in question, one of {left, right, drop, rotate, menu, restart, quit}.
                 * @returns {Boolean} True if the user is inputting this command.
                 */
                checkCommand: function(command, pressedKeys) {
                    return !!(command.filter((key) => pressedKeys[key]).length);
                }
            };


            // High score board manipulation.
            let highScoreStoreKey = "highScores";
            let newHighScore = function() {
                return {name: "-", score: 0, colour: 15}
            }
            let highScores;
            try {
                highScores = JSON.parse(window.localStorage.getItem(highScoreStoreKey));
            } catch(e) {
                highScores = false;
            }
            highScores = highScores || Array(options.menu.numHighScores).fill().map(newHighScore);
            while (highScores.length < options.menu.numHighScores) {
                highScores.push(newHighScore());
            }
            while (highScores.length > options.menu.numHighScores) {
                highScores.pop();
            }

            options.highScoreUtils = {
                /**
                 * Checks whether a score is a high-score.
                 * @param {Number} score - The score to check.
                 */
                isHighScore: function(score) {
                    return !highScores[highScores.length-1] || highScores[highScores.length-1].score < score;
                },
                /**
                 * Adds a high score to the leaderboard and to local storage.
                 * @param {String} name - The player name to submit.
                 * @param {Number} score - The score to submit.
                 * @param {PaletteIndex=} colour - A colour to display the score as in the leaderboard.
                 */
                submitHighScore: function(name, score, colour) {
                    if (name.length) {
                        let highScore = {
                            name: name,
                            score: score,
                            colour: colour == null ? 15 : colour
                        };
                        highScores = highScores.concat(highScore).sort((a,b) => b.score - a.score).slice(0, options.menu.numHighScores);
                        window.localStorage.setItem(highScoreStoreKey, JSON.stringify(highScores));
                    }
                },
                /**
                 * Erase all high score records.
                 */
                clearHighScores: function() {
                    highScores = Array(options.menu.numHighScores).fill().map(newHighScore);
                    window.localStorage.removeItem(highScoreStoreKey);
                },
                /**
                 * Get a deep copy of the high scores.
                 * @returns {highScore[]} A list of high score entries.
                 */
                getHighScores: function() {
                    return JSON.parse(JSON.stringify(highScores));
                }
            }

            resolve(options);
        }).catch((error) => console.error(error));
    }).catch((error) => console.error(error));
})(window.setup = window.setup || {});