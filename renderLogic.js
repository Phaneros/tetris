"use strict";

/**
 * Defines functions relating to translating a gameState object into
 * The necessary vertex array data to render the game.
 */
(function(setup) {
    setup.gameRenderSetup = new Promise((resolve) => {
        Promise.all([
            resources.loadTileSheetInfo,
            setup.optionsSetup,
            setup.displayUtilsSetup
        ]).then((inputs) => {
            let [tileSheet, options, displayUtils] = inputs;

            // Setup measurements
            let sprite = options.sprite;
            let normalizedPalette = options.normalizedPalette;
            // let stringDisplayUtils = options.stringDisplayUtils;

            /**
             * Get a new renderData object to load with data to shaders.
             * @returns {RenderData} An empty renderData object.
             */
            let newRenderData = function() {
                return {
                    vertices: [],
                    texCoords: [],
                    paletteIndices: [],
                    fadeFactors: [],
                    numTriangles: 0
                };
            };

            /**
             * Convert a sprite index into cornerCoords.
             * @param {!SpriteIndex} spriteIndex - The index into the tilesheet of the desired sprite.
             * @param {[Number, Number]=} size - If the desired sprite is multiple tiles large,
             * specifies the [x,y] size of the sprite.
             * @returns {CornerCoords} The four coordinates of the corners of the box. 
             */
            let spriteIndex2TextureCoords = function(spriteIndex, size) {
                let topLeft = [
                    spriteIndex[0] * sprite.tile.w,
                    spriteIndex[1] * sprite.tile.h
                ];
                let bottomRight;
                if (size != null) {
                    bottomRight = [
                        (spriteIndex[0] + size[0]) * sprite.tile.w,
                        (spriteIndex[1] + size[1]) * sprite.tile.h
                    ];
                } else {
                    bottomRight = [
                        (spriteIndex[0] + 1) * sprite.tile.w,
                        (spriteIndex[1] + 1) * sprite.tile.h
                    ];
                }
                return {
                    topLeft: vec.addvec(topLeft, sprite.tile.adjustTop, sprite.tile.adjustLeft),
                    topRight: vec.addvec([bottomRight[0], topLeft[1]], sprite.tile.adjustTop, sprite.tile.adjustRight),
                    bottomLeft: vec.addvec([topLeft[0], bottomRight[1]], sprite.tile.adjustLeft),
                    bottomRight: vec.addvec(bottomRight, sprite.tile.adjustRight)
                };
            };

            /**
             * Convert a canvas position into cornerCoords representing a box.
             * @param {!Coordinate} topLeft The top-left canvas coordinates of the desired box.
             * @param {!Vector} topLeft The top-left canvas coordinates of the desired box.
             * @returns {CornerCoords} The four coordinates of the corners of the box. 
             */
            let canvasPosition2CanvasCoords = function(topLeft, rectSpan) {
                return {
                    topLeft: topLeft,
                    topRight: vec.addvec(topLeft, [rectSpan[0], 0]),
                    bottomLeft: vec.addvec(topLeft, [0, -rectSpan[1]]),
                    bottomRight: vec.addvec(topLeft, [rectSpan[0], -rectSpan[1]])
                };
            };

            /**
             * Converts a paletteIndex into a 
             * @param {!PaletteIndex} paletteIndex - The identifier of the desired palette.
             * @returns {Number} The y-coordinate of the palette image where the palette may be found.
             */
            let paletteIndex2Coord = function(paletteIndex) {
                return paletteIndex * normalizedPalette.pixel.h;
            };

            /**
             * Updates the renderData structure with information to draw a single square.
             * @param {!RenderData} renderData - The renderData object to hold all the buffer data.
             * @param {!Coordinate} canvasPosition - The canvas position of the top-left of the square.
             * @param {!SpriteIndex} spriteIndex - The tile ID within the tileSheet to draw the square with.
             * @param {!Number[]} rectSize - The side lengths of the rectangle, in [x, y] format.
             * @param {!PaletteIndex} paletteIndex - The palette to use.
             * @param {!Number} fade - The fade factor to use.
             * @param {SpriteIndex=} bottomRightSpriteIndex - For multi-tile sprites, specifies the bottom-right most tile index.
             */
            let drawRectangle = function(renderData, canvasPosition, spriteIndex, rectSize, paletteIndex, fade, bottomRightSpriteIndex) {
                let spriteCoords = spriteIndex2TextureCoords(spriteIndex, bottomRightSpriteIndex);
                let canvasCoords = canvasPosition2CanvasCoords(canvasPosition, rectSize);

                // top-left triangle
                Array.prototype.push.apply(renderData.vertices,
                    canvasCoords.topLeft
                    .concat(canvasCoords.topRight)
                    .concat(canvasCoords.bottomLeft)
                );
                Array.prototype.push.apply(renderData.texCoords,
                    spriteCoords.topLeft
                    .concat(spriteCoords.topRight)
                    .concat(spriteCoords.bottomLeft)
                );

                // bottom-right triangle
                Array.prototype.push.apply(renderData.vertices,
                    canvasCoords.bottomRight
                    .concat(canvasCoords.topRight)
                    .concat(canvasCoords.bottomLeft)
                );
                Array.prototype.push.apply(renderData.texCoords,
                    spriteCoords.bottomRight
                    .concat(spriteCoords.topRight)
                    .concat(spriteCoords.bottomLeft)
                );
                
                Array.prototype.push.apply(renderData.paletteIndices,
                    Array(6).fill(paletteIndex2Coord(paletteIndex))
                );

                Array.prototype.push.apply(renderData.fadeFactors,
                    Array(6).fill(fade)
                );

                renderData.numTriangles += 2;
            };

            let tileRender = {
                /**
                 * Checks over the string formatting options and calls the corresponding tileRender method.
                 * @param {RenderData} renderData - The renderData object to load buffer data into.
                 * @param {TileIndex[][]} tiles - The tile indices to render.
                 * @param {RenderOptions} opts - The options on how to render the string.
                 */
                drawTiles: function(renderData, tiles, opts) {
                    opts.palette = tileRender.unpackPalettes(opts.palette, [tiles.length, tiles[0].length])
                    tileRender._default(renderData, tiles, opts);
                },

                /**
                 * Checks over a palette option field and expands special qualifiers.
                 * @param {PaletteIndex|PaletteIndex[]|PaletteIndex[][]} palette - The unexpanded palette option.
                 * @param {[Number, Number]]} boardDimensions - The dimensions of the board.
                 * @returns {PaletteIndex|PaletteIndex[][]} The expanded version of the palette option.
                 */
                unpackPalettes: function(palette, boardDimensions) {
                    if (typeof palette === "string") {
                        let special = palette.match(/(Rainbow)(\d+)(?:,(\d+))?/i);
                        return tileRender[special[1].toLowerCase()](boardDimensions, special.slice(2).map((x) => parseInt(x)));
                    }
                    return palette;
                },

                /**
                 * Renders a list of tiles with options to to a renderData object.
                 * @param {RenderData} renderData - The renderData object to load buffer data into.
                 * @param {TileIndex[][]} tiles - The tile indices to render.
                 * @param {RenderOptions} opts - The options on how to render the tiles.
                 */
                _default: function(renderData, tiles, opts) {
                    for (let i = 0; i < tiles.length; ++i) {
                        let paletteRow = Array.isArray(opts.palette) ? opts.palette[i] : opts.palette;
                        let fadeRow = Array.isArray(opts.fade) ? opts.fade[i] : opts.fade;
                        for (let j = 0; j < tiles[i].length; ++j) {
                            if (tiles[i][j]) {
                                let palette = Array.isArray(paletteRow) ? paletteRow[j] : paletteRow;
                                let fade = Array.isArray(fadeRow) ? fadeRow[j] : fadeRow;
                                drawRectangle(
                                    renderData,
                                    vec.addvec(opts.position, [j*opts.fontSize.dx, -i*opts.fontSize.dy]),
                                    tiles[i][j],
                                    [opts.fontSize.w, opts.fontSize.h],
                                    palette == options.unassignedPalette ? 15 : palette,
                                    fade);
                            }
                        }
                    }
                },

                /**
                 * 
                 * @param {[Number, Numer]} boardDimensions - The tile indices to render.
                 * @param {[Number, Number]} args - [paletteSize, paletteStart].
                 * @returns {PaletteIndex[]} The resulting palette indices.
                 */
                rainbow: function(boardDimensions, args) {
                    let paletteStart = args[1] || 0;
                    let paletteSize = args[0] || 7;
                    let retval = [];
                    let currentPalette = paletteStart;
                    for (let row = 0; row < boardDimensions[0]; ++row) {
                        retval.push([]);
                        for(let col = 0; col < boardDimensions[1]; ++col) {
                            retval[row].push(currentPalette);
                            currentPalette = (currentPalette + 1 - paletteStart) % paletteSize + paletteStart;
                        }
                    }
                    return retval;
                }
            };

            /**
             * Renders a forattedString object to a renderData object.
             * @param {RenderData} renderData - The renderData object to load buffer data into.
             * @param {String|String[]} string - The text to render.
             * @param {RenderOptions} string - The options on how to render the string.
             */
            let drawFormattedString = function(renderData, string, opts) {
                if (!Array.isArray(string)) {
                    string = [string];
                }
                displayUtils.formatRenderOptions(opts);
                let tiles = string.map((x) => displayUtils.string2SpriteIndices(x));
                tileRender.drawTiles(renderData, tiles, opts);
            }

            /**
             * Draws a big title card
             * @param {RenderData} renderData 
             * @param {GameState|MenuState} state 
             */
            let drawTitleCard = function(renderData, state) {
                if (!state.titleCard.options) {
                    state.titleCard.options = displayUtils.getDisplayOptions("title");
                }
                displayUtils.formatRenderOptions(state.titleCard.options)
                drawRectangle(
                    renderData, state.titleCard.options.position, state.titleCard.tileIndex[0],
                    [state.titleCard.options.fontSize.w, state.titleCard.options.fontSize.h],
                    state.titleCard.options.palette,
                    state.titleCard.options.fade,
                    state.titleCard.tileIndex[1]
                );
            }

            /**
             * Draws the board.
             * @param {RenderData} renderData - The RenderData structure to add information to. 
             * @param {GameState} gameState - The current game state.
             */
            let drawBoard = function(renderData, board) {
                let [tiles, palettes, fades] = displayUtils.board2SpriteIndicesAndPalettes(vec.transposemat(board.tiles).reverse());
                let opts = {
                    position: board.options.position || displayUtils.getDisplayOptions("board").position,
                    fontSize: board.options.fontSize || displayUtils.getDisplayOptions("board").fontSize
                };

                // get defaults
                let defaultOpts = board.options || displayUtils.getDisplayOptions("board");
                if (Array.isArray(defaultOpts.palette)) {
                    defaultOpts.palette = defaultOpts.palette[0];
                }
                if (Array.isArray(defaultOpts.fade)) {
                    defaultOpts.fade = defaultOpts.fade[0];
                }

                // adjust options
                displayUtils.formatRenderOptions(opts);
                opts.palette = palettes.map((outer) => outer.map((inner) => inner === options.unassignedPalette ? defaultOpts.palette : inner));
                opts.fade = fades.map((outer) => outer.map((inner) => inner === -1 ? defaultOpts.fade : inner));
                tileRender.drawTiles(renderData, tiles, opts);
            };

            /**
             * Draws a preview (empty tiles have no outlines).
             * @param {RenderData} renderData - The RenderData structure to add information to. 
             * @param {GameBoard} board - The current game state.
             */
            let drawPreview = function(renderData, board) {
                let [tiles, palettes] = displayUtils.board2SpriteIndicesAndPalettes(vec.transposemat(board.tiles).reverse());
                let opts = board.options || displayUtils.getDisplayOptions("board");

                // filter out tiles that are empty, based off their tile sprite
                tiles = tiles.map((x) => x.map(
                    (tile) =>   vec.equalv(tile, options.game.emptyTileSprite) ?
                                false : tile));

                displayUtils.formatRenderOptions(opts);
                opts.palette = palettes;
                tileRender.drawTiles(renderData, tiles, opts);
            };

            /**
             * Draws the score display.
             * @param {!RenderData} renderData - The RenderData structure to add information to. 
             * @param {!GameState} gameState - The current game state.
             */
            let drawScore = function(renderData, gameState) {
                let scoreStrings = gameState.stats.text;
                let opts = gameState.stats.options || displayUtils.getDisplayOptions("scoreboard");
                drawFormattedString(renderData, scoreStrings, opts);
            };

            /**
             * Draws the log.
             * @param {!RenderData} renderData - The RenderData structure to add information to.
             * @param {!GameState|MenuState} state - The current gameState.
             */
            let drawLog = function(renderData, state) {
                drawFormattedString(renderData, state.log.text, state.log.options || displayUtils.getDisplayOptions("log"));
            };

            /**
             * Draws the background.
             * @param {!RenderData} renderData - The RenderData structure to add information to.
             */
            let drawBackground = function(renderData) {
                drawRectangle(
                    renderData, [-1, 1],
                    tileSheet.tileMap.BACKGROUND.coord,
                    [2, 2],
                    14,
                    0.3,
                    tileSheet.tileMap.BACKGROUND.size
                );
            };

            /**
             * Draws the menu.
             * @param {!RenderData} renderData - The RenderData structure to add information to.
             * @param {!MenuState} menuState - The current menu state.
             */
            let drawMenu = function(renderData, menuState) {
                let menuOpts = menuState.header.options || displayUtils.getDisplayOptions("sideMenu");
                let [currentX, currentY] = menuOpts.position;

                // get render options for title and subtitle
                menuOpts.title = displayUtils.formatRenderOptions(menuOpts.title);
                menuOpts.subtitle = menuOpts.subtitle || {};
                displayUtils.inheritRenderOptions(menuOpts.subtitle, menuOpts.title || {});
                displayUtils.formatRenderOptions(menuOpts.subtitle);
                
                // draw title
                if (menuState.header.title != null) {
                    menuOpts.title.position = [currentX, currentY];
                    drawFormattedString(renderData, menuState.header.title, menuOpts.title);
                    if (Array.isArray(menuState.header.title)) {
                        currentY -= menuOpts.title.fontSize.dy * menuState.header.title.length;
                    } else {
                        currentY -= menuOpts.title.fontSize.dy;
                    }
                }

                // draw subtitle
                if (menuState.header.subtitle != null) {
                    menuOpts.subtitle.position = [currentX, currentY];
                    drawFormattedString(renderData, menuState.header.subtitle, menuOpts.subtitle);
                }
                // add subtitle spacing even if there is no subtitle.
                currentY -= menuOpts.subtitle.fontSize.dy;

                // draw body text
                if (menuState.header.body != null) {
                    menuOpts.body = menuOpts.body || {};
                    displayUtils.inheritRenderOptions(menuOpts.menuItems, menuOpts.subtitle || menuOpts.title || {});
                    displayUtils.formatRenderOptions(menuOpts.body);
                    menuOpts.body.position = [currentX, currentY];
                    drawFormattedString(renderData, menuState.header.body, menuOpts.body);
                    currentY = Infinity;
                }

                // draw menu items
                if (menuState.menuItems != null) {
                    menuOpts.menuItems = menuOpts.menuItems || {};
                    displayUtils.inheritRenderOptions(menuOpts.menuItems, menuOpts.subtitle || menuOpts.title || {});
                    displayUtils.formatRenderOptions(menuOpts.menuItems);
                    if (currentY === Infinity) {
                        currentY = currentX + menuOpts.menuItems.fontSize.dy * (menuState.menuItems.length + 0.5);
                    }
                    menuOpts.menuItems.position = [currentX + menuOpts.menuItems.fontSize.dx * 1.5, currentY];
                    drawFormattedString(renderData, menuState.menuItems.map((x) => x.displayText), menuOpts.menuItems);

                    // draw cursor
                    if (menuState.cursorPosition != null) {
                        menuOpts.cursor = menuOpts.cursor || {};
                        displayUtils.inheritRenderOptions(menuOpts.cursor, menuOpts.menuItems || {});
                        displayUtils.formatRenderOptions(menuOpts.cursor);
                        menuOpts.cursor.position = [currentX, currentY - menuState.cursorPosition * menuOpts.menuItems.fontSize.dy];
                        drawFormattedString(renderData, ">", menuOpts.cursor);
                    }
                }

            }

            let render = {
                /**
                 * Returns
                 * @param {!GameState|MenuState} state - The application state to draw.
                 * @returns {RenderData} A RenderData object containing all vertex attribute information 
                 * needed to draw the current state.
                 */
                getRenderData: function(state) {
                    let renderData = newRenderData();

                    if (state.titleCard) {
                        drawTitleCard(renderData, state)
                    }
                    if (state.board) {
                        drawBoard(renderData, state.board);
                    }
                    if (state.preview) {
                        drawPreview(renderData, state.preview);
                    }
                    if (state.header) {
                        drawMenu(renderData, state);
                    }
                    if (state.stats && state.stats.text) {
                        drawScore(renderData, state);
                    }
                    if (state.log && state.log.text) {
                        drawLog(renderData, state);
                    }
                    drawBackground(renderData);

                    return renderData;
                }
            };

            resolve(render);
        }).catch((error) => console.error(error));
    }).catch((error) => console.error(error));
})(window.setup = window.setup || {});
