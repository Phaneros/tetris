
/**
 * Loads all resource and shader files, storing the outputs in promises.
 */
(function(resources) {
    var tetrominosPath = 'Resources/tetrominos.json';
    var tileSheetPath = 'Resources/TetrisBlocks.png';
    var palettePath = 'Resources/Palette.png';
    var tileSheetInfoPath = 'Resources/tileSheetInfo.json';
    var displayOptionsPath = 'Resources/displayOptions.json';
    var vertShaderPath = 'Shaders/shader1.vert';
    var fragShaderPath = 'Shaders/shader1.frag';

    // load tetrominos
    resources.loadTetrominos = new Promise((resolve) => {
        fileUtils.loadJson(tetrominosPath, (contents) => {
            resources.tetrominos = contents;
            resolve(contents);
        });
    }).catch((error) => console.error(error));

    // load tileSheet image
    resources.loadTileSheet = new Promise((resolve) => {
        var img = new Image();
        img.src = tileSheetPath;
        img.onload = () => {
            resolve(img);
        };
    }).catch((error) => console.error(error));

    // load palette image
    resources.loadPalette = new Promise((resolve) => {
        var img = new Image();
        img.src = palettePath;
        img.onload = () => {
            resolve(img);
        };
    }).catch((error) => console.error(error));

    // load tile sheet info
    resources.loadTileSheetInfo = new Promise((resolve) => {
        fileUtils.loadJson(tileSheetInfoPath, (contents) => {
            resolve(contents);
        });
    }).catch((error) => console.error(error));

    // load tile sheet info
    resources.loadDisplayOptions = new Promise((resolve) => {
        fileUtils.loadJson(displayOptionsPath, (contents) => {
            resolve(contents);
        });
    }).catch((error) => console.error(error));

    // load vertex shader text
    resources.loadVertShader = new Promise((resolve) => {
        fileUtils.loadTextDocument(vertShaderPath, (contents) => {
            resolve(contents);
        });
    }).catch((error) => console.error(error));

    // load fragment shader text
    resources.loadFragShader = new Promise((resolve) => {
        fileUtils.loadTextDocument(fragShaderPath, (contents) => {
            resolve(contents);
        });
    }).catch((error) => console.error(error));

    Promise.all([
        resources.loadTetrominos,
        resources.loadTileSheet,
        resources.loadPalette,
        resources.loadTileSheetInfo,
        resources.loadVertShader,
        resources.loadFragShader
    ]).then(
        () => {
            console.log("all resources loaded successfully");
        }
    )

})(window.resources = window.resources || {});