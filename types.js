
/**
 *  A list of numbers that identify a point in a space.
 * @typedef {Array.<Number>} Coordinate - An n-length array specifying an n-dimensional position.
 * 
 * @typedef {Array.<Number>} Vector - A 1-dimensional list of numbers.
 * 
 * A coordinate measured in tiles for which tile to use from the sprite sheet.
 * @typedef {Coordinate} SpriteIndex - A 2-length array identifying a tile in the sprite sheet.
 * 
 * An identifier for a palette.
 * @typedef {Number} PaletteIndex - An identifier for which palette to use from the palette image.
 * 
 * A game frame identifier.
 * @typedef {Number} Frame - The frame number.
 * 
 * A definition for a font.
 * @typedef {Object} FontSize
 * @property {"pixels"|"normalized"} mode - Specifies whether to treat values as pixels distances or normalized values.
 * @property {Number} w - A character width.
 * @property {Number} h - A character height
 * @property {Number} dx - The x coordinate difference between lines.
 * @property {Number} dy - The y coordinate difference between lines.
 * 
 * A container for a formatted line of text.
 * @typedef {Object} RenderOptions
 * @property {Coordinate} position - The top-left position of the top line of text.
 * @property {FontSize} fontSize - The [x,y] size of each character.
 * @property {PaletteIndex|PaletteIndex[]|PaletteIndex[][]} palette - The palette index for this string
 * 
 * A container for all the numbers to be loaded into buffers for rendering.
 * @typedef {Object} RenderData
 * @property {Array.<Number>} vertices - The vertex coordinates, flattened.
 * @property {Array.<Number>} texCoords - The texture coordinates, flattened.
 * @property {Array.<Number>} paletteIndices - The palette indices, flattened.
 * @property {Number} numTriangles - The number of triangles that can be drawn with the current data.
 * 
 * A container of four coordinates to be used to define a rectangle.
 * @typedef {Object} CornerCoords
 * @property {Coordinate} topLeft - The top-left corner coordinates.
 * @property {Coordinate} topRight - The top-right corner coordinates.
 * @property {Coordinate} bottomLeft - The bottom-left corner coordinates.
 * @property {Coordinate} bottomRight - The bottom-right corner coordinates.
 * 
 * A structure representing a tile's properties, from tileSheetInfo.json.
 * @typedef {Object} TileInfo 
 * @property {SpriteIndex} coord - The spriteIndex of the associated tile. 
 * @property {string=} regex - A regex string to check against a local map to see if this tile will fit a position.
 * 
 * A structure containing all information pertaining to a tetris piece that is currently falling.
 * @typedef {Object} ActivePiece
 * @property {PieceType} pieceType - Information about the piece's properties.
 * @property {Coordinate} position - The piece's current position.
 * @property {Array.<Coordinate>} tileDeltas - A list of the piece's tile coordinates, relative to the piece's position.
 * @property {Array.<SpriteIndex>} tileSprites - A list of sprite indices that correspond to each tile.
 * @property {PaletteIndex} paletteIndex - The piece's palette.
 * @property {Frame} lastFall - The last frame when this piece dropped one tile.
 * @property {Frame} lastRotation - The last frame when this piece rotated.
 * @property {Frame} lastShift - The last frame when this piece moved sideways.
 * 
 * A container for a board tile's information.
 * @typedef {Object} BoardTile
 * @property {SpriteIndex} spriteIndex - The tile's sprite.
 * @property {PaletteIndex} paletteIndex - The tile's palette.
 * @property {Boolean} filled - Flags if the the tile is occupied by a dropped piece. 
 * 
 * A container of a game's stats.
 * @typedef {Object} GameStats
 * @property {Number} score - The game's score.
 * @property {Number} level - The game's current level.
 * @property {Number} piecesLanded - A count of how many pieces fell and were solidifed.
 * @property {Number} rows - A count of how many rows were completed and disappeared.
 * @property {String} text - The text to be displayed in the score display.
 * @property {RenderOptions} style - The text display options to render the score with.
 * 
 * A structure containing styled text.
 * @typedef {Object} MenuText
 * @property {RenderOptions=} options - The string format to be used.
 * @property {String|String[]=} title - The title text
 * @property {String|String[]=} subtitle - The subtitle text
 * @property {String|String[]=} body - The body text
 * 
 * A structure for a game board and associated display data.
 * @typedef {Object} GameBoard
 * @property {RenderOptions} options - The display options for the board.
 * @property {Object[][]} tiles - The actual tiles of the board
 * 
 * A structure defining how a log is displayed.
 * @typedef {Object} Log
 * @property {Number} lastUpdate - The last frame this log was written to.
 * @property {String} text - The text to be displayed.
 * @property {RenderOptions=} options - The options to display the log with. 
 * 
 * A structure containing all the information pertaining to a menu's state.
 * @typedef {Object} MenuState
 * @property {Object=} titleCard - The title card for a main screen menu.
 * @property {Object=} log - An extra line of text.
 * @property {MenuText} header - The text that will be displayed.
 * @property {Number=} textInputIndex - The index onto the textInputField stack for user input.
 * @property {Number=} cursorPosition - The index of the currently selected menu item.
 * @property {Number=} lastCursorMove - The last time when the cursor moved.
 * @property {Number} lastMenuSelect - The last time when a menu item was selected.
 * @property {"full"|"side"} menuType - Flags whether the menu takes up the whole screen or just the side.
 * 
 * A structure containing all the information pertaining to the game's current state.
 * @typedef {Object} GameState
 * @property {GameBoard} board - The collection of board tiles.
 * @property {GameBoard} preview - The tiles of the upcoming piece preview.
 * @property {ActivePiece} activePiece - The piece that is currently falling.
 * @property {Number} fallingSpeed - The number of frames before the actice piece will try to drop 1 tile.
 * @property {Frame} currentFrame - The current frame number.
 * @property {Boolean} gameOver - Flags whether the game is in a lost state.
 * @property {GameStats} stats - The current game stats.
 * @property {Log} log - Container for messages to be displayed in the log.
 * @property {Boolean} highScore - Flags if the game finished with a high score.
 * 
 * A structure for wrapping all menu and game state information.
 * @typedef {Object} AppState
 * @property {GameState=} gameState - The game state component of the application.
 * @property {MenuState=} menuState - The menu state component of the application.
 * @property {Object} pressedKeys - A map of the currently pressed keys.
 * 
 */
